<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BlogCategoryTranslationApiTest extends TestCase
{
    use MakeBlogCategoryTranslationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBlogCategoryTranslation()
    {
        $blogCategoryTranslation = $this->fakeBlogCategoryTranslationData();
        $this->json('POST', '/api/v1/blogCategoryTranslations', $blogCategoryTranslation);

        $this->assertApiResponse($blogCategoryTranslation);
    }

    /**
     * @test
     */
    public function testReadBlogCategoryTranslation()
    {
        $blogCategoryTranslation = $this->makeBlogCategoryTranslation();
        $this->json('GET', '/api/v1/blogCategoryTranslations/'.$blogCategoryTranslation->id);

        $this->assertApiResponse($blogCategoryTranslation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBlogCategoryTranslation()
    {
        $blogCategoryTranslation = $this->makeBlogCategoryTranslation();
        $editedBlogCategoryTranslation = $this->fakeBlogCategoryTranslationData();

        $this->json('PUT', '/api/v1/blogCategoryTranslations/'.$blogCategoryTranslation->id, $editedBlogCategoryTranslation);

        $this->assertApiResponse($editedBlogCategoryTranslation);
    }

    /**
     * @test
     */
    public function testDeleteBlogCategoryTranslation()
    {
        $blogCategoryTranslation = $this->makeBlogCategoryTranslation();
        $this->json('DELETE', '/api/v1/blogCategoryTranslations/'.$blogCategoryTranslation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/blogCategoryTranslations/'.$blogCategoryTranslation->id);

        $this->assertResponseStatus(404);
    }
}
