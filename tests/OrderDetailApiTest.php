<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrderDetailApiTest extends TestCase
{
    use MakeOrderDetailTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateOrderDetail()
    {
        $orderDetail = $this->fakeOrderDetailData();
        $this->json('POST', '/api/v1/orderDetails', $orderDetail);

        $this->assertApiResponse($orderDetail);
    }

    /**
     * @test
     */
    public function testReadOrderDetail()
    {
        $orderDetail = $this->makeOrderDetail();
        $this->json('GET', '/api/v1/orderDetails/'.$orderDetail->id);

        $this->assertApiResponse($orderDetail->toArray());
    }

    /**
     * @test
     */
    public function testUpdateOrderDetail()
    {
        $orderDetail = $this->makeOrderDetail();
        $editedOrderDetail = $this->fakeOrderDetailData();

        $this->json('PUT', '/api/v1/orderDetails/'.$orderDetail->id, $editedOrderDetail);

        $this->assertApiResponse($editedOrderDetail);
    }

    /**
     * @test
     */
    public function testDeleteOrderDetail()
    {
        $orderDetail = $this->makeOrderDetail();
        $this->json('DELETE', '/api/v1/orderDetails/'.$orderDetail->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/orderDetails/'.$orderDetail->id);

        $this->assertResponseStatus(404);
    }
}
