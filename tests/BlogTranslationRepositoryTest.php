<?php

use App\Models\Admin\BlogTranslation;
use App\Repositories\Admin\BlogTranslationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BlogTranslationRepositoryTest extends TestCase
{
    use MakeBlogTranslationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BlogTranslationRepository
     */
    protected $blogTranslationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->blogTranslationRepo = App::make(BlogTranslationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBlogTranslation()
    {
        $blogTranslation = $this->fakeBlogTranslationData();
        $createdBlogTranslation = $this->blogTranslationRepo->create($blogTranslation);
        $createdBlogTranslation = $createdBlogTranslation->toArray();
        $this->assertArrayHasKey('id', $createdBlogTranslation);
        $this->assertNotNull($createdBlogTranslation['id'], 'Created BlogTranslation must have id specified');
        $this->assertNotNull(BlogTranslation::find($createdBlogTranslation['id']), 'BlogTranslation with given id must be in DB');
        $this->assertModelData($blogTranslation, $createdBlogTranslation);
    }

    /**
     * @test read
     */
    public function testReadBlogTranslation()
    {
        $blogTranslation = $this->makeBlogTranslation();
        $dbBlogTranslation = $this->blogTranslationRepo->find($blogTranslation->id);
        $dbBlogTranslation = $dbBlogTranslation->toArray();
        $this->assertModelData($blogTranslation->toArray(), $dbBlogTranslation);
    }

    /**
     * @test update
     */
    public function testUpdateBlogTranslation()
    {
        $blogTranslation = $this->makeBlogTranslation();
        $fakeBlogTranslation = $this->fakeBlogTranslationData();
        $updatedBlogTranslation = $this->blogTranslationRepo->update($fakeBlogTranslation, $blogTranslation->id);
        $this->assertModelData($fakeBlogTranslation, $updatedBlogTranslation->toArray());
        $dbBlogTranslation = $this->blogTranslationRepo->find($blogTranslation->id);
        $this->assertModelData($fakeBlogTranslation, $dbBlogTranslation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBlogTranslation()
    {
        $blogTranslation = $this->makeBlogTranslation();
        $resp = $this->blogTranslationRepo->delete($blogTranslation->id);
        $this->assertTrue($resp);
        $this->assertNull(BlogTranslation::find($blogTranslation->id), 'BlogTranslation should not exist in DB');
    }
}
