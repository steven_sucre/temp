<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ActivityTranslationApiTest extends TestCase
{
    use MakeActivityTranslationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateActivityTranslation()
    {
        $activityTranslation = $this->fakeActivityTranslationData();
        $this->json('POST', '/api/v1/activityTranslations', $activityTranslation);

        $this->assertApiResponse($activityTranslation);
    }

    /**
     * @test
     */
    public function testReadActivityTranslation()
    {
        $activityTranslation = $this->makeActivityTranslation();
        $this->json('GET', '/api/v1/activityTranslations/'.$activityTranslation->id);

        $this->assertApiResponse($activityTranslation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateActivityTranslation()
    {
        $activityTranslation = $this->makeActivityTranslation();
        $editedActivityTranslation = $this->fakeActivityTranslationData();

        $this->json('PUT', '/api/v1/activityTranslations/'.$activityTranslation->id, $editedActivityTranslation);

        $this->assertApiResponse($editedActivityTranslation);
    }

    /**
     * @test
     */
    public function testDeleteActivityTranslation()
    {
        $activityTranslation = $this->makeActivityTranslation();
        $this->json('DELETE', '/api/v1/activityTranslations/'.$activityTranslation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/activityTranslations/'.$activityTranslation->id);

        $this->assertResponseStatus(404);
    }
}
