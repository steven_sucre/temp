<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoomTranslationApiTest extends TestCase
{
    use MakeRoomTranslationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateRoomTranslation()
    {
        $roomTranslation = $this->fakeRoomTranslationData();
        $this->json('POST', '/api/v1/roomTranslations', $roomTranslation);

        $this->assertApiResponse($roomTranslation);
    }

    /**
     * @test
     */
    public function testReadRoomTranslation()
    {
        $roomTranslation = $this->makeRoomTranslation();
        $this->json('GET', '/api/v1/roomTranslations/'.$roomTranslation->id);

        $this->assertApiResponse($roomTranslation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateRoomTranslation()
    {
        $roomTranslation = $this->makeRoomTranslation();
        $editedRoomTranslation = $this->fakeRoomTranslationData();

        $this->json('PUT', '/api/v1/roomTranslations/'.$roomTranslation->id, $editedRoomTranslation);

        $this->assertApiResponse($editedRoomTranslation);
    }

    /**
     * @test
     */
    public function testDeleteRoomTranslation()
    {
        $roomTranslation = $this->makeRoomTranslation();
        $this->json('DELETE', '/api/v1/roomTranslations/'.$roomTranslation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/roomTranslations/'.$roomTranslation->id);

        $this->assertResponseStatus(404);
    }
}
