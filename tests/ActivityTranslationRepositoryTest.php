<?php

use App\Models\Admin\ActivityTranslation;
use App\Repositories\Admin\ActivityTranslationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ActivityTranslationRepositoryTest extends TestCase
{
    use MakeActivityTranslationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ActivityTranslationRepository
     */
    protected $activityTranslationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->activityTranslationRepo = App::make(ActivityTranslationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateActivityTranslation()
    {
        $activityTranslation = $this->fakeActivityTranslationData();
        $createdActivityTranslation = $this->activityTranslationRepo->create($activityTranslation);
        $createdActivityTranslation = $createdActivityTranslation->toArray();
        $this->assertArrayHasKey('id', $createdActivityTranslation);
        $this->assertNotNull($createdActivityTranslation['id'], 'Created ActivityTranslation must have id specified');
        $this->assertNotNull(ActivityTranslation::find($createdActivityTranslation['id']), 'ActivityTranslation with given id must be in DB');
        $this->assertModelData($activityTranslation, $createdActivityTranslation);
    }

    /**
     * @test read
     */
    public function testReadActivityTranslation()
    {
        $activityTranslation = $this->makeActivityTranslation();
        $dbActivityTranslation = $this->activityTranslationRepo->find($activityTranslation->id);
        $dbActivityTranslation = $dbActivityTranslation->toArray();
        $this->assertModelData($activityTranslation->toArray(), $dbActivityTranslation);
    }

    /**
     * @test update
     */
    public function testUpdateActivityTranslation()
    {
        $activityTranslation = $this->makeActivityTranslation();
        $fakeActivityTranslation = $this->fakeActivityTranslationData();
        $updatedActivityTranslation = $this->activityTranslationRepo->update($fakeActivityTranslation, $activityTranslation->id);
        $this->assertModelData($fakeActivityTranslation, $updatedActivityTranslation->toArray());
        $dbActivityTranslation = $this->activityTranslationRepo->find($activityTranslation->id);
        $this->assertModelData($fakeActivityTranslation, $dbActivityTranslation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteActivityTranslation()
    {
        $activityTranslation = $this->makeActivityTranslation();
        $resp = $this->activityTranslationRepo->delete($activityTranslation->id);
        $this->assertTrue($resp);
        $this->assertNull(ActivityTranslation::find($activityTranslation->id), 'ActivityTranslation should not exist in DB');
    }
}
