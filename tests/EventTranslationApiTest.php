<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EventTranslationApiTest extends TestCase
{
    use MakeEventTranslationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateEventTranslation()
    {
        $eventTranslation = $this->fakeEventTranslationData();
        $this->json('POST', '/api/v1/eventTranslations', $eventTranslation);

        $this->assertApiResponse($eventTranslation);
    }

    /**
     * @test
     */
    public function testReadEventTranslation()
    {
        $eventTranslation = $this->makeEventTranslation();
        $this->json('GET', '/api/v1/eventTranslations/'.$eventTranslation->id);

        $this->assertApiResponse($eventTranslation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateEventTranslation()
    {
        $eventTranslation = $this->makeEventTranslation();
        $editedEventTranslation = $this->fakeEventTranslationData();

        $this->json('PUT', '/api/v1/eventTranslations/'.$eventTranslation->id, $editedEventTranslation);

        $this->assertApiResponse($editedEventTranslation);
    }

    /**
     * @test
     */
    public function testDeleteEventTranslation()
    {
        $eventTranslation = $this->makeEventTranslation();
        $this->json('DELETE', '/api/v1/eventTranslations/'.$eventTranslation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/eventTranslations/'.$eventTranslation->id);

        $this->assertResponseStatus(404);
    }
}
