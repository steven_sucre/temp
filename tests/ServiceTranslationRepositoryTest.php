<?php

use App\Models\Admin\ServiceTranslation;
use App\Repositories\Admin\ServiceTranslationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ServiceTranslationRepositoryTest extends TestCase
{
    use MakeServiceTranslationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ServiceTranslationRepository
     */
    protected $serviceTranslationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->serviceTranslationRepo = App::make(ServiceTranslationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateServiceTranslation()
    {
        $serviceTranslation = $this->fakeServiceTranslationData();
        $createdServiceTranslation = $this->serviceTranslationRepo->create($serviceTranslation);
        $createdServiceTranslation = $createdServiceTranslation->toArray();
        $this->assertArrayHasKey('id', $createdServiceTranslation);
        $this->assertNotNull($createdServiceTranslation['id'], 'Created ServiceTranslation must have id specified');
        $this->assertNotNull(ServiceTranslation::find($createdServiceTranslation['id']), 'ServiceTranslation with given id must be in DB');
        $this->assertModelData($serviceTranslation, $createdServiceTranslation);
    }

    /**
     * @test read
     */
    public function testReadServiceTranslation()
    {
        $serviceTranslation = $this->makeServiceTranslation();
        $dbServiceTranslation = $this->serviceTranslationRepo->find($serviceTranslation->id);
        $dbServiceTranslation = $dbServiceTranslation->toArray();
        $this->assertModelData($serviceTranslation->toArray(), $dbServiceTranslation);
    }

    /**
     * @test update
     */
    public function testUpdateServiceTranslation()
    {
        $serviceTranslation = $this->makeServiceTranslation();
        $fakeServiceTranslation = $this->fakeServiceTranslationData();
        $updatedServiceTranslation = $this->serviceTranslationRepo->update($fakeServiceTranslation, $serviceTranslation->id);
        $this->assertModelData($fakeServiceTranslation, $updatedServiceTranslation->toArray());
        $dbServiceTranslation = $this->serviceTranslationRepo->find($serviceTranslation->id);
        $this->assertModelData($fakeServiceTranslation, $dbServiceTranslation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteServiceTranslation()
    {
        $serviceTranslation = $this->makeServiceTranslation();
        $resp = $this->serviceTranslationRepo->delete($serviceTranslation->id);
        $this->assertTrue($resp);
        $this->assertNull(ServiceTranslation::find($serviceTranslation->id), 'ServiceTranslation should not exist in DB');
    }
}
