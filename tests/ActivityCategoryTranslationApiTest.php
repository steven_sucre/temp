<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ActivityCategoryTranslationApiTest extends TestCase
{
    use MakeActivityCategoryTranslationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateActivityCategoryTranslation()
    {
        $activityCategoryTranslation = $this->fakeActivityCategoryTranslationData();
        $this->json('POST', '/api/v1/activityCategoryTranslations', $activityCategoryTranslation);

        $this->assertApiResponse($activityCategoryTranslation);
    }

    /**
     * @test
     */
    public function testReadActivityCategoryTranslation()
    {
        $activityCategoryTranslation = $this->makeActivityCategoryTranslation();
        $this->json('GET', '/api/v1/activityCategoryTranslations/'.$activityCategoryTranslation->id);

        $this->assertApiResponse($activityCategoryTranslation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateActivityCategoryTranslation()
    {
        $activityCategoryTranslation = $this->makeActivityCategoryTranslation();
        $editedActivityCategoryTranslation = $this->fakeActivityCategoryTranslationData();

        $this->json('PUT', '/api/v1/activityCategoryTranslations/'.$activityCategoryTranslation->id, $editedActivityCategoryTranslation);

        $this->assertApiResponse($editedActivityCategoryTranslation);
    }

    /**
     * @test
     */
    public function testDeleteActivityCategoryTranslation()
    {
        $activityCategoryTranslation = $this->makeActivityCategoryTranslation();
        $this->json('DELETE', '/api/v1/activityCategoryTranslations/'.$activityCategoryTranslation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/activityCategoryTranslations/'.$activityCategoryTranslation->id);

        $this->assertResponseStatus(404);
    }
}
