<?php

use App\Models\Admin\ProductSubcategoryTranslation;
use App\Repositories\Admin\ProductSubcategoryTranslationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductSubcategoryTranslationRepositoryTest extends TestCase
{
    use MakeProductSubcategoryTranslationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProductSubcategoryTranslationRepository
     */
    protected $productSubcategoryTranslationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->productSubcategoryTranslationRepo = App::make(ProductSubcategoryTranslationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateProductSubcategoryTranslation()
    {
        $productSubcategoryTranslation = $this->fakeProductSubcategoryTranslationData();
        $createdProductSubcategoryTranslation = $this->productSubcategoryTranslationRepo->create($productSubcategoryTranslation);
        $createdProductSubcategoryTranslation = $createdProductSubcategoryTranslation->toArray();
        $this->assertArrayHasKey('id', $createdProductSubcategoryTranslation);
        $this->assertNotNull($createdProductSubcategoryTranslation['id'], 'Created ProductSubcategoryTranslation must have id specified');
        $this->assertNotNull(ProductSubcategoryTranslation::find($createdProductSubcategoryTranslation['id']), 'ProductSubcategoryTranslation with given id must be in DB');
        $this->assertModelData($productSubcategoryTranslation, $createdProductSubcategoryTranslation);
    }

    /**
     * @test read
     */
    public function testReadProductSubcategoryTranslation()
    {
        $productSubcategoryTranslation = $this->makeProductSubcategoryTranslation();
        $dbProductSubcategoryTranslation = $this->productSubcategoryTranslationRepo->find($productSubcategoryTranslation->id);
        $dbProductSubcategoryTranslation = $dbProductSubcategoryTranslation->toArray();
        $this->assertModelData($productSubcategoryTranslation->toArray(), $dbProductSubcategoryTranslation);
    }

    /**
     * @test update
     */
    public function testUpdateProductSubcategoryTranslation()
    {
        $productSubcategoryTranslation = $this->makeProductSubcategoryTranslation();
        $fakeProductSubcategoryTranslation = $this->fakeProductSubcategoryTranslationData();
        $updatedProductSubcategoryTranslation = $this->productSubcategoryTranslationRepo->update($fakeProductSubcategoryTranslation, $productSubcategoryTranslation->id);
        $this->assertModelData($fakeProductSubcategoryTranslation, $updatedProductSubcategoryTranslation->toArray());
        $dbProductSubcategoryTranslation = $this->productSubcategoryTranslationRepo->find($productSubcategoryTranslation->id);
        $this->assertModelData($fakeProductSubcategoryTranslation, $dbProductSubcategoryTranslation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteProductSubcategoryTranslation()
    {
        $productSubcategoryTranslation = $this->makeProductSubcategoryTranslation();
        $resp = $this->productSubcategoryTranslationRepo->delete($productSubcategoryTranslation->id);
        $this->assertTrue($resp);
        $this->assertNull(ProductSubcategoryTranslation::find($productSubcategoryTranslation->id), 'ProductSubcategoryTranslation should not exist in DB');
    }
}
