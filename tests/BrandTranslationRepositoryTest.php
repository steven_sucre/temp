<?php

use App\Models\Admin\BrandTranslation;
use App\Repositories\Admin\BrandTranslationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BrandTranslationRepositoryTest extends TestCase
{
    use MakeBrandTranslationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BrandTranslationRepository
     */
    protected $brandTranslationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->brandTranslationRepo = App::make(BrandTranslationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBrandTranslation()
    {
        $brandTranslation = $this->fakeBrandTranslationData();
        $createdBrandTranslation = $this->brandTranslationRepo->create($brandTranslation);
        $createdBrandTranslation = $createdBrandTranslation->toArray();
        $this->assertArrayHasKey('id', $createdBrandTranslation);
        $this->assertNotNull($createdBrandTranslation['id'], 'Created BrandTranslation must have id specified');
        $this->assertNotNull(BrandTranslation::find($createdBrandTranslation['id']), 'BrandTranslation with given id must be in DB');
        $this->assertModelData($brandTranslation, $createdBrandTranslation);
    }

    /**
     * @test read
     */
    public function testReadBrandTranslation()
    {
        $brandTranslation = $this->makeBrandTranslation();
        $dbBrandTranslation = $this->brandTranslationRepo->find($brandTranslation->id);
        $dbBrandTranslation = $dbBrandTranslation->toArray();
        $this->assertModelData($brandTranslation->toArray(), $dbBrandTranslation);
    }

    /**
     * @test update
     */
    public function testUpdateBrandTranslation()
    {
        $brandTranslation = $this->makeBrandTranslation();
        $fakeBrandTranslation = $this->fakeBrandTranslationData();
        $updatedBrandTranslation = $this->brandTranslationRepo->update($fakeBrandTranslation, $brandTranslation->id);
        $this->assertModelData($fakeBrandTranslation, $updatedBrandTranslation->toArray());
        $dbBrandTranslation = $this->brandTranslationRepo->find($brandTranslation->id);
        $this->assertModelData($fakeBrandTranslation, $dbBrandTranslation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBrandTranslation()
    {
        $brandTranslation = $this->makeBrandTranslation();
        $resp = $this->brandTranslationRepo->delete($brandTranslation->id);
        $this->assertTrue($resp);
        $this->assertNull(BrandTranslation::find($brandTranslation->id), 'BrandTranslation should not exist in DB');
    }
}
