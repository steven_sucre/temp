<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EventCategoryTranslationApiTest extends TestCase
{
    use MakeEventCategoryTranslationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateEventCategoryTranslation()
    {
        $eventCategoryTranslation = $this->fakeEventCategoryTranslationData();
        $this->json('POST', '/api/v1/eventCategoryTranslations', $eventCategoryTranslation);

        $this->assertApiResponse($eventCategoryTranslation);
    }

    /**
     * @test
     */
    public function testReadEventCategoryTranslation()
    {
        $eventCategoryTranslation = $this->makeEventCategoryTranslation();
        $this->json('GET', '/api/v1/eventCategoryTranslations/'.$eventCategoryTranslation->id);

        $this->assertApiResponse($eventCategoryTranslation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateEventCategoryTranslation()
    {
        $eventCategoryTranslation = $this->makeEventCategoryTranslation();
        $editedEventCategoryTranslation = $this->fakeEventCategoryTranslationData();

        $this->json('PUT', '/api/v1/eventCategoryTranslations/'.$eventCategoryTranslation->id, $editedEventCategoryTranslation);

        $this->assertApiResponse($editedEventCategoryTranslation);
    }

    /**
     * @test
     */
    public function testDeleteEventCategoryTranslation()
    {
        $eventCategoryTranslation = $this->makeEventCategoryTranslation();
        $this->json('DELETE', '/api/v1/eventCategoryTranslations/'.$eventCategoryTranslation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/eventCategoryTranslations/'.$eventCategoryTranslation->id);

        $this->assertResponseStatus(404);
    }
}
