<?php

use App\Models\Admin\ProductTranslation;
use App\Repositories\Admin\ProductTranslationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductTranslationRepositoryTest extends TestCase
{
    use MakeProductTranslationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProductTranslationRepository
     */
    protected $productTranslationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->productTranslationRepo = App::make(ProductTranslationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateProductTranslation()
    {
        $productTranslation = $this->fakeProductTranslationData();
        $createdProductTranslation = $this->productTranslationRepo->create($productTranslation);
        $createdProductTranslation = $createdProductTranslation->toArray();
        $this->assertArrayHasKey('id', $createdProductTranslation);
        $this->assertNotNull($createdProductTranslation['id'], 'Created ProductTranslation must have id specified');
        $this->assertNotNull(ProductTranslation::find($createdProductTranslation['id']), 'ProductTranslation with given id must be in DB');
        $this->assertModelData($productTranslation, $createdProductTranslation);
    }

    /**
     * @test read
     */
    public function testReadProductTranslation()
    {
        $productTranslation = $this->makeProductTranslation();
        $dbProductTranslation = $this->productTranslationRepo->find($productTranslation->id);
        $dbProductTranslation = $dbProductTranslation->toArray();
        $this->assertModelData($productTranslation->toArray(), $dbProductTranslation);
    }

    /**
     * @test update
     */
    public function testUpdateProductTranslation()
    {
        $productTranslation = $this->makeProductTranslation();
        $fakeProductTranslation = $this->fakeProductTranslationData();
        $updatedProductTranslation = $this->productTranslationRepo->update($fakeProductTranslation, $productTranslation->id);
        $this->assertModelData($fakeProductTranslation, $updatedProductTranslation->toArray());
        $dbProductTranslation = $this->productTranslationRepo->find($productTranslation->id);
        $this->assertModelData($fakeProductTranslation, $dbProductTranslation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteProductTranslation()
    {
        $productTranslation = $this->makeProductTranslation();
        $resp = $this->productTranslationRepo->delete($productTranslation->id);
        $this->assertTrue($resp);
        $this->assertNull(ProductTranslation::find($productTranslation->id), 'ProductTranslation should not exist in DB');
    }
}
