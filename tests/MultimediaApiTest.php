<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MultimediaApiTest extends TestCase
{
    use MakeMultimediaTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateMultimedia()
    {
        $multimedia = $this->fakeMultimediaData();
        $this->json('POST', '/api/v1/multimedia', $multimedia);

        $this->assertApiResponse($multimedia);
    }

    /**
     * @test
     */
    public function testReadMultimedia()
    {
        $multimedia = $this->makeMultimedia();
        $this->json('GET', '/api/v1/multimedia/'.$multimedia->id);

        $this->assertApiResponse($multimedia->toArray());
    }

    /**
     * @test
     */
    public function testUpdateMultimedia()
    {
        $multimedia = $this->makeMultimedia();
        $editedMultimedia = $this->fakeMultimediaData();

        $this->json('PUT', '/api/v1/multimedia/'.$multimedia->id, $editedMultimedia);

        $this->assertApiResponse($editedMultimedia);
    }

    /**
     * @test
     */
    public function testDeleteMultimedia()
    {
        $multimedia = $this->makeMultimedia();
        $this->json('DELETE', '/api/v1/multimedia/'.$multimedia->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/multimedia/'.$multimedia->id);

        $this->assertResponseStatus(404);
    }
}
