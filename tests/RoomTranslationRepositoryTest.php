<?php

use App\Models\Admin\RoomTranslation;
use App\Repositories\Admin\RoomTranslationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoomTranslationRepositoryTest extends TestCase
{
    use MakeRoomTranslationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RoomTranslationRepository
     */
    protected $roomTranslationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->roomTranslationRepo = App::make(RoomTranslationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateRoomTranslation()
    {
        $roomTranslation = $this->fakeRoomTranslationData();
        $createdRoomTranslation = $this->roomTranslationRepo->create($roomTranslation);
        $createdRoomTranslation = $createdRoomTranslation->toArray();
        $this->assertArrayHasKey('id', $createdRoomTranslation);
        $this->assertNotNull($createdRoomTranslation['id'], 'Created RoomTranslation must have id specified');
        $this->assertNotNull(RoomTranslation::find($createdRoomTranslation['id']), 'RoomTranslation with given id must be in DB');
        $this->assertModelData($roomTranslation, $createdRoomTranslation);
    }

    /**
     * @test read
     */
    public function testReadRoomTranslation()
    {
        $roomTranslation = $this->makeRoomTranslation();
        $dbRoomTranslation = $this->roomTranslationRepo->find($roomTranslation->id);
        $dbRoomTranslation = $dbRoomTranslation->toArray();
        $this->assertModelData($roomTranslation->toArray(), $dbRoomTranslation);
    }

    /**
     * @test update
     */
    public function testUpdateRoomTranslation()
    {
        $roomTranslation = $this->makeRoomTranslation();
        $fakeRoomTranslation = $this->fakeRoomTranslationData();
        $updatedRoomTranslation = $this->roomTranslationRepo->update($fakeRoomTranslation, $roomTranslation->id);
        $this->assertModelData($fakeRoomTranslation, $updatedRoomTranslation->toArray());
        $dbRoomTranslation = $this->roomTranslationRepo->find($roomTranslation->id);
        $this->assertModelData($fakeRoomTranslation, $dbRoomTranslation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteRoomTranslation()
    {
        $roomTranslation = $this->makeRoomTranslation();
        $resp = $this->roomTranslationRepo->delete($roomTranslation->id);
        $this->assertTrue($resp);
        $this->assertNull(RoomTranslation::find($roomTranslation->id), 'RoomTranslation should not exist in DB');
    }
}
