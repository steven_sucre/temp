<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BookingDetailApiTest extends TestCase
{
    use MakeBookingDetailTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBookingDetail()
    {
        $bookingDetail = $this->fakeBookingDetailData();
        $this->json('POST', '/api/v1/bookingDetails', $bookingDetail);

        $this->assertApiResponse($bookingDetail);
    }

    /**
     * @test
     */
    public function testReadBookingDetail()
    {
        $bookingDetail = $this->makeBookingDetail();
        $this->json('GET', '/api/v1/bookingDetails/'.$bookingDetail->id);

        $this->assertApiResponse($bookingDetail->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBookingDetail()
    {
        $bookingDetail = $this->makeBookingDetail();
        $editedBookingDetail = $this->fakeBookingDetailData();

        $this->json('PUT', '/api/v1/bookingDetails/'.$bookingDetail->id, $editedBookingDetail);

        $this->assertApiResponse($editedBookingDetail);
    }

    /**
     * @test
     */
    public function testDeleteBookingDetail()
    {
        $bookingDetail = $this->makeBookingDetail();
        $this->json('DELETE', '/api/v1/bookingDetails/'.$bookingDetail->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/bookingDetails/'.$bookingDetail->id);

        $this->assertResponseStatus(404);
    }
}
