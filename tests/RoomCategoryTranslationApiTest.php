<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoomCategoryTranslationApiTest extends TestCase
{
    use MakeRoomCategoryTranslationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateRoomCategoryTranslation()
    {
        $roomCategoryTranslation = $this->fakeRoomCategoryTranslationData();
        $this->json('POST', '/api/v1/roomCategoryTranslations', $roomCategoryTranslation);

        $this->assertApiResponse($roomCategoryTranslation);
    }

    /**
     * @test
     */
    public function testReadRoomCategoryTranslation()
    {
        $roomCategoryTranslation = $this->makeRoomCategoryTranslation();
        $this->json('GET', '/api/v1/roomCategoryTranslations/'.$roomCategoryTranslation->id);

        $this->assertApiResponse($roomCategoryTranslation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateRoomCategoryTranslation()
    {
        $roomCategoryTranslation = $this->makeRoomCategoryTranslation();
        $editedRoomCategoryTranslation = $this->fakeRoomCategoryTranslationData();

        $this->json('PUT', '/api/v1/roomCategoryTranslations/'.$roomCategoryTranslation->id, $editedRoomCategoryTranslation);

        $this->assertApiResponse($editedRoomCategoryTranslation);
    }

    /**
     * @test
     */
    public function testDeleteRoomCategoryTranslation()
    {
        $roomCategoryTranslation = $this->makeRoomCategoryTranslation();
        $this->json('DELETE', '/api/v1/roomCategoryTranslations/'.$roomCategoryTranslation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/roomCategoryTranslations/'.$roomCategoryTranslation->id);

        $this->assertResponseStatus(404);
    }
}
