<?php

use App\Models\Admin\EventTranslation;
use App\Repositories\Admin\EventTranslationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EventTranslationRepositoryTest extends TestCase
{
    use MakeEventTranslationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var EventTranslationRepository
     */
    protected $eventTranslationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->eventTranslationRepo = App::make(EventTranslationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateEventTranslation()
    {
        $eventTranslation = $this->fakeEventTranslationData();
        $createdEventTranslation = $this->eventTranslationRepo->create($eventTranslation);
        $createdEventTranslation = $createdEventTranslation->toArray();
        $this->assertArrayHasKey('id', $createdEventTranslation);
        $this->assertNotNull($createdEventTranslation['id'], 'Created EventTranslation must have id specified');
        $this->assertNotNull(EventTranslation::find($createdEventTranslation['id']), 'EventTranslation with given id must be in DB');
        $this->assertModelData($eventTranslation, $createdEventTranslation);
    }

    /**
     * @test read
     */
    public function testReadEventTranslation()
    {
        $eventTranslation = $this->makeEventTranslation();
        $dbEventTranslation = $this->eventTranslationRepo->find($eventTranslation->id);
        $dbEventTranslation = $dbEventTranslation->toArray();
        $this->assertModelData($eventTranslation->toArray(), $dbEventTranslation);
    }

    /**
     * @test update
     */
    public function testUpdateEventTranslation()
    {
        $eventTranslation = $this->makeEventTranslation();
        $fakeEventTranslation = $this->fakeEventTranslationData();
        $updatedEventTranslation = $this->eventTranslationRepo->update($fakeEventTranslation, $eventTranslation->id);
        $this->assertModelData($fakeEventTranslation, $updatedEventTranslation->toArray());
        $dbEventTranslation = $this->eventTranslationRepo->find($eventTranslation->id);
        $this->assertModelData($fakeEventTranslation, $dbEventTranslation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteEventTranslation()
    {
        $eventTranslation = $this->makeEventTranslation();
        $resp = $this->eventTranslationRepo->delete($eventTranslation->id);
        $this->assertTrue($resp);
        $this->assertNull(EventTranslation::find($eventTranslation->id), 'EventTranslation should not exist in DB');
    }
}
