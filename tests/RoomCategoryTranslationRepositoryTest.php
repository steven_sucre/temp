<?php

use App\Models\Admin\RoomCategoryTranslation;
use App\Repositories\Admin\RoomCategoryTranslationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoomCategoryTranslationRepositoryTest extends TestCase
{
    use MakeRoomCategoryTranslationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RoomCategoryTranslationRepository
     */
    protected $roomCategoryTranslationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->roomCategoryTranslationRepo = App::make(RoomCategoryTranslationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateRoomCategoryTranslation()
    {
        $roomCategoryTranslation = $this->fakeRoomCategoryTranslationData();
        $createdRoomCategoryTranslation = $this->roomCategoryTranslationRepo->create($roomCategoryTranslation);
        $createdRoomCategoryTranslation = $createdRoomCategoryTranslation->toArray();
        $this->assertArrayHasKey('id', $createdRoomCategoryTranslation);
        $this->assertNotNull($createdRoomCategoryTranslation['id'], 'Created RoomCategoryTranslation must have id specified');
        $this->assertNotNull(RoomCategoryTranslation::find($createdRoomCategoryTranslation['id']), 'RoomCategoryTranslation with given id must be in DB');
        $this->assertModelData($roomCategoryTranslation, $createdRoomCategoryTranslation);
    }

    /**
     * @test read
     */
    public function testReadRoomCategoryTranslation()
    {
        $roomCategoryTranslation = $this->makeRoomCategoryTranslation();
        $dbRoomCategoryTranslation = $this->roomCategoryTranslationRepo->find($roomCategoryTranslation->id);
        $dbRoomCategoryTranslation = $dbRoomCategoryTranslation->toArray();
        $this->assertModelData($roomCategoryTranslation->toArray(), $dbRoomCategoryTranslation);
    }

    /**
     * @test update
     */
    public function testUpdateRoomCategoryTranslation()
    {
        $roomCategoryTranslation = $this->makeRoomCategoryTranslation();
        $fakeRoomCategoryTranslation = $this->fakeRoomCategoryTranslationData();
        $updatedRoomCategoryTranslation = $this->roomCategoryTranslationRepo->update($fakeRoomCategoryTranslation, $roomCategoryTranslation->id);
        $this->assertModelData($fakeRoomCategoryTranslation, $updatedRoomCategoryTranslation->toArray());
        $dbRoomCategoryTranslation = $this->roomCategoryTranslationRepo->find($roomCategoryTranslation->id);
        $this->assertModelData($fakeRoomCategoryTranslation, $dbRoomCategoryTranslation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteRoomCategoryTranslation()
    {
        $roomCategoryTranslation = $this->makeRoomCategoryTranslation();
        $resp = $this->roomCategoryTranslationRepo->delete($roomCategoryTranslation->id);
        $this->assertTrue($resp);
        $this->assertNull(RoomCategoryTranslation::find($roomCategoryTranslation->id), 'RoomCategoryTranslation should not exist in DB');
    }
}
