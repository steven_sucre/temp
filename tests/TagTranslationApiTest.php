<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TagTranslationApiTest extends TestCase
{
    use MakeTagTranslationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateTagTranslation()
    {
        $tagTranslation = $this->fakeTagTranslationData();
        $this->json('POST', '/api/v1/tagTranslations', $tagTranslation);

        $this->assertApiResponse($tagTranslation);
    }

    /**
     * @test
     */
    public function testReadTagTranslation()
    {
        $tagTranslation = $this->makeTagTranslation();
        $this->json('GET', '/api/v1/tagTranslations/'.$tagTranslation->id);

        $this->assertApiResponse($tagTranslation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateTagTranslation()
    {
        $tagTranslation = $this->makeTagTranslation();
        $editedTagTranslation = $this->fakeTagTranslationData();

        $this->json('PUT', '/api/v1/tagTranslations/'.$tagTranslation->id, $editedTagTranslation);

        $this->assertApiResponse($editedTagTranslation);
    }

    /**
     * @test
     */
    public function testDeleteTagTranslation()
    {
        $tagTranslation = $this->makeTagTranslation();
        $this->json('DELETE', '/api/v1/tagTranslations/'.$tagTranslation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/tagTranslations/'.$tagTranslation->id);

        $this->assertResponseStatus(404);
    }
}
