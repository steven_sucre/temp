<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BrandTranslationApiTest extends TestCase
{
    use MakeBrandTranslationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBrandTranslation()
    {
        $brandTranslation = $this->fakeBrandTranslationData();
        $this->json('POST', '/api/v1/brandTranslations', $brandTranslation);

        $this->assertApiResponse($brandTranslation);
    }

    /**
     * @test
     */
    public function testReadBrandTranslation()
    {
        $brandTranslation = $this->makeBrandTranslation();
        $this->json('GET', '/api/v1/brandTranslations/'.$brandTranslation->id);

        $this->assertApiResponse($brandTranslation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBrandTranslation()
    {
        $brandTranslation = $this->makeBrandTranslation();
        $editedBrandTranslation = $this->fakeBrandTranslationData();

        $this->json('PUT', '/api/v1/brandTranslations/'.$brandTranslation->id, $editedBrandTranslation);

        $this->assertApiResponse($editedBrandTranslation);
    }

    /**
     * @test
     */
    public function testDeleteBrandTranslation()
    {
        $brandTranslation = $this->makeBrandTranslation();
        $this->json('DELETE', '/api/v1/brandTranslations/'.$brandTranslation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/brandTranslations/'.$brandTranslation->id);

        $this->assertResponseStatus(404);
    }
}
