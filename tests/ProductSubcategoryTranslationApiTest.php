<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductSubcategoryTranslationApiTest extends TestCase
{
    use MakeProductSubcategoryTranslationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateProductSubcategoryTranslation()
    {
        $productSubcategoryTranslation = $this->fakeProductSubcategoryTranslationData();
        $this->json('POST', '/api/v1/productSubcategoryTranslations', $productSubcategoryTranslation);

        $this->assertApiResponse($productSubcategoryTranslation);
    }

    /**
     * @test
     */
    public function testReadProductSubcategoryTranslation()
    {
        $productSubcategoryTranslation = $this->makeProductSubcategoryTranslation();
        $this->json('GET', '/api/v1/productSubcategoryTranslations/'.$productSubcategoryTranslation->id);

        $this->assertApiResponse($productSubcategoryTranslation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateProductSubcategoryTranslation()
    {
        $productSubcategoryTranslation = $this->makeProductSubcategoryTranslation();
        $editedProductSubcategoryTranslation = $this->fakeProductSubcategoryTranslationData();

        $this->json('PUT', '/api/v1/productSubcategoryTranslations/'.$productSubcategoryTranslation->id, $editedProductSubcategoryTranslation);

        $this->assertApiResponse($editedProductSubcategoryTranslation);
    }

    /**
     * @test
     */
    public function testDeleteProductSubcategoryTranslation()
    {
        $productSubcategoryTranslation = $this->makeProductSubcategoryTranslation();
        $this->json('DELETE', '/api/v1/productSubcategoryTranslations/'.$productSubcategoryTranslation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/productSubcategoryTranslations/'.$productSubcategoryTranslation->id);

        $this->assertResponseStatus(404);
    }
}
