<?php

use App\Models\Admin\Multimedia;
use App\Repositories\Admin\MultimediaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MultimediaRepositoryTest extends TestCase
{
    use MakeMultimediaTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var MultimediaRepository
     */
    protected $multimediaRepo;

    public function setUp()
    {
        parent::setUp();
        $this->multimediaRepo = App::make(MultimediaRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateMultimedia()
    {
        $multimedia = $this->fakeMultimediaData();
        $createdMultimedia = $this->multimediaRepo->create($multimedia);
        $createdMultimedia = $createdMultimedia->toArray();
        $this->assertArrayHasKey('id', $createdMultimedia);
        $this->assertNotNull($createdMultimedia['id'], 'Created Multimedia must have id specified');
        $this->assertNotNull(Multimedia::find($createdMultimedia['id']), 'Multimedia with given id must be in DB');
        $this->assertModelData($multimedia, $createdMultimedia);
    }

    /**
     * @test read
     */
    public function testReadMultimedia()
    {
        $multimedia = $this->makeMultimedia();
        $dbMultimedia = $this->multimediaRepo->find($multimedia->id);
        $dbMultimedia = $dbMultimedia->toArray();
        $this->assertModelData($multimedia->toArray(), $dbMultimedia);
    }

    /**
     * @test update
     */
    public function testUpdateMultimedia()
    {
        $multimedia = $this->makeMultimedia();
        $fakeMultimedia = $this->fakeMultimediaData();
        $updatedMultimedia = $this->multimediaRepo->update($fakeMultimedia, $multimedia->id);
        $this->assertModelData($fakeMultimedia, $updatedMultimedia->toArray());
        $dbMultimedia = $this->multimediaRepo->find($multimedia->id);
        $this->assertModelData($fakeMultimedia, $dbMultimedia->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteMultimedia()
    {
        $multimedia = $this->makeMultimedia();
        $resp = $this->multimediaRepo->delete($multimedia->id);
        $this->assertTrue($resp);
        $this->assertNull(Multimedia::find($multimedia->id), 'Multimedia should not exist in DB');
    }
}
