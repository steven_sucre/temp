<?php

use Faker\Factory as Faker;
use App\Models\Admin\BlogCategoryTranslation;
use App\Repositories\Admin\BlogCategoryTranslationRepository;

trait MakeBlogCategoryTranslationTrait
{
    /**
     * Create fake instance of BlogCategoryTranslation and save it in database
     *
     * @param array $blogCategoryTranslationFields
     * @return BlogCategoryTranslation
     */
    public function makeBlogCategoryTranslation($blogCategoryTranslationFields = [])
    {
        /** @var BlogCategoryTranslationRepository $blogCategoryTranslationRepo */
        $blogCategoryTranslationRepo = App::make(BlogCategoryTranslationRepository::class);
        $theme = $this->fakeBlogCategoryTranslationData($blogCategoryTranslationFields);
        return $blogCategoryTranslationRepo->create($theme);
    }

    /**
     * Get fake instance of BlogCategoryTranslation
     *
     * @param array $blogCategoryTranslationFields
     * @return BlogCategoryTranslation
     */
    public function fakeBlogCategoryTranslation($blogCategoryTranslationFields = [])
    {
        return new BlogCategoryTranslation($this->fakeBlogCategoryTranslationData($blogCategoryTranslationFields));
    }

    /**
     * Get fake data of BlogCategoryTranslation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBlogCategoryTranslationData($blogCategoryTranslationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'blog_category_id' => $fake->randomDigitNotNull,
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $blogCategoryTranslationFields);
    }
}
