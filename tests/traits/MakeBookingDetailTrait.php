<?php

use Faker\Factory as Faker;
use App\Models\Admin\BookingDetail;
use App\Repositories\Admin\BookingDetailRepository;

trait MakeBookingDetailTrait
{
    /**
     * Create fake instance of BookingDetail and save it in database
     *
     * @param array $bookingDetailFields
     * @return BookingDetail
     */
    public function makeBookingDetail($bookingDetailFields = [])
    {
        /** @var BookingDetailRepository $bookingDetailRepo */
        $bookingDetailRepo = App::make(BookingDetailRepository::class);
        $theme = $this->fakeBookingDetailData($bookingDetailFields);
        return $bookingDetailRepo->create($theme);
    }

    /**
     * Get fake instance of BookingDetail
     *
     * @param array $bookingDetailFields
     * @return BookingDetail
     */
    public function fakeBookingDetail($bookingDetailFields = [])
    {
        return new BookingDetail($this->fakeBookingDetailData($bookingDetailFields));
    }

    /**
     * Get fake data of BookingDetail
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBookingDetailData($bookingDetailFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'checkin_date' => $fake->word,
            'checkout_date' => $fake->word,
            'persons_amount' => $fake->randomDigitNotNull,
            'booking_id' => $fake->randomDigitNotNull,
            'row_id' => $fake->randomDigitNotNull,
            'payment_method_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $bookingDetailFields);
    }
}
