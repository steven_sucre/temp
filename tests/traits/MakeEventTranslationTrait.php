<?php

use Faker\Factory as Faker;
use App\Models\Admin\EventTranslation;
use App\Repositories\Admin\EventTranslationRepository;

trait MakeEventTranslationTrait
{
    /**
     * Create fake instance of EventTranslation and save it in database
     *
     * @param array $eventTranslationFields
     * @return EventTranslation
     */
    public function makeEventTranslation($eventTranslationFields = [])
    {
        /** @var EventTranslationRepository $eventTranslationRepo */
        $eventTranslationRepo = App::make(EventTranslationRepository::class);
        $theme = $this->fakeEventTranslationData($eventTranslationFields);
        return $eventTranslationRepo->create($theme);
    }

    /**
     * Get fake instance of EventTranslation
     *
     * @param array $eventTranslationFields
     * @return EventTranslation
     */
    public function fakeEventTranslation($eventTranslationFields = [])
    {
        return new EventTranslation($this->fakeEventTranslationData($eventTranslationFields));
    }

    /**
     * Get fake data of EventTranslation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeEventTranslationData($eventTranslationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'event_id' => $fake->randomDigitNotNull,
            'language_id' => $fake->randomDigitNotNull,
            'title' => $fake->word,
            'subtitle' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $eventTranslationFields);
    }
}
