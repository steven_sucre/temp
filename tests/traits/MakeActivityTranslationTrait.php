<?php

use Faker\Factory as Faker;
use App\Models\Admin\ActivityTranslation;
use App\Repositories\Admin\ActivityTranslationRepository;

trait MakeActivityTranslationTrait
{
    /**
     * Create fake instance of ActivityTranslation and save it in database
     *
     * @param array $activityTranslationFields
     * @return ActivityTranslation
     */
    public function makeActivityTranslation($activityTranslationFields = [])
    {
        /** @var ActivityTranslationRepository $activityTranslationRepo */
        $activityTranslationRepo = App::make(ActivityTranslationRepository::class);
        $theme = $this->fakeActivityTranslationData($activityTranslationFields);
        return $activityTranslationRepo->create($theme);
    }

    /**
     * Get fake instance of ActivityTranslation
     *
     * @param array $activityTranslationFields
     * @return ActivityTranslation
     */
    public function fakeActivityTranslation($activityTranslationFields = [])
    {
        return new ActivityTranslation($this->fakeActivityTranslationData($activityTranslationFields));
    }

    /**
     * Get fake data of ActivityTranslation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeActivityTranslationData($activityTranslationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'activity_id' => $fake->randomDigitNotNull,
            'language_id' => $fake->randomDigitNotNull,
            'title' => $fake->word,
            'subtitle' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $activityTranslationFields);
    }
}
