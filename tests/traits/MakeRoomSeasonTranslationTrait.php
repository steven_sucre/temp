<?php

use Faker\Factory as Faker;
use App\Models\Admin\RoomSeasonTranslation;
use App\Repositories\Admin\RoomSeasonTranslationRepository;

trait MakeRoomSeasonTranslationTrait
{
    /**
     * Create fake instance of RoomSeasonTranslation and save it in database
     *
     * @param array $roomSeasonTranslationFields
     * @return RoomSeasonTranslation
     */
    public function makeRoomSeasonTranslation($roomSeasonTranslationFields = [])
    {
        /** @var RoomSeasonTranslationRepository $roomSeasonTranslationRepo */
        $roomSeasonTranslationRepo = App::make(RoomSeasonTranslationRepository::class);
        $theme = $this->fakeRoomSeasonTranslationData($roomSeasonTranslationFields);
        return $roomSeasonTranslationRepo->create($theme);
    }

    /**
     * Get fake instance of RoomSeasonTranslation
     *
     * @param array $roomSeasonTranslationFields
     * @return RoomSeasonTranslation
     */
    public function fakeRoomSeasonTranslation($roomSeasonTranslationFields = [])
    {
        return new RoomSeasonTranslation($this->fakeRoomSeasonTranslationData($roomSeasonTranslationFields));
    }

    /**
     * Get fake data of RoomSeasonTranslation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRoomSeasonTranslationData($roomSeasonTranslationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'room_season_id' => $fake->randomDigitNotNull,
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $roomSeasonTranslationFields);
    }
}
