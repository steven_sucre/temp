<?php

use Faker\Factory as Faker;
use App\Models\Admin\SeoTranslation;
use App\Repositories\Admin\SeoTranslationRepository;

trait MakeSeoTranslationTrait
{
    /**
     * Create fake instance of SeoTranslation and save it in database
     *
     * @param array $seoTranslationFields
     * @return SeoTranslation
     */
    public function makeSeoTranslation($seoTranslationFields = [])
    {
        /** @var SeoTranslationRepository $seoTranslationRepo */
        $seoTranslationRepo = App::make(SeoTranslationRepository::class);
        $theme = $this->fakeSeoTranslationData($seoTranslationFields);
        return $seoTranslationRepo->create($theme);
    }

    /**
     * Get fake instance of SeoTranslation
     *
     * @param array $seoTranslationFields
     * @return SeoTranslation
     */
    public function fakeSeoTranslation($seoTranslationFields = [])
    {
        return new SeoTranslation($this->fakeSeoTranslationData($seoTranslationFields));
    }

    /**
     * Get fake data of SeoTranslation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSeoTranslationData($seoTranslationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'description' => $fake->word,
            'author' => $fake->word,
            'robots' => $fake->word,
            'subject' => $fake->word,
            'language' => $fake->word,
            'keywords' => $fake->text,
            'seo_id' => $fake->randomDigitNotNull,
            'language_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $seoTranslationFields);
    }
}
