<?php

use Faker\Factory as Faker;
use App\Models\Admin\Request;
use App\Repositories\Admin\RequestRepository;

trait MakeRequestTrait
{
    /**
     * Create fake instance of Request and save it in database
     *
     * @param array $requestFields
     * @return Request
     */
    public function makeRequest($requestFields = [])
    {
        /** @var RequestRepository $requestRepo */
        $requestRepo = App::make(RequestRepository::class);
        $theme = $this->fakeRequestData($requestFields);
        return $requestRepo->create($theme);
    }

    /**
     * Get fake instance of Request
     *
     * @param array $requestFields
     * @return Request
     */
    public function fakeRequest($requestFields = [])
    {
        return new Request($this->fakeRequestData($requestFields));
    }

    /**
     * Get fake data of Request
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRequestData($requestFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'checkin_date' => $fake->word,
            'checkout_date' => $fake->word,
            'persons_amount' => $fake->randomDigitNotNull,
            'user_id' => $fake->randomDigitNotNull,
            'no_register_user_name' => $fake->word,
            'no_register_user_email' => $fake->word,
            'no_register_user_phone' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $requestFields);
    }
}
