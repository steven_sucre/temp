<?php

use Faker\Factory as Faker;
use App\Models\Admin\ProductCategoryTranslation;
use App\Repositories\Admin\ProductCategoryTranslationRepository;

trait MakeProductCategoryTranslationTrait
{
    /**
     * Create fake instance of ProductCategoryTranslation and save it in database
     *
     * @param array $productCategoryTranslationFields
     * @return ProductCategoryTranslation
     */
    public function makeProductCategoryTranslation($productCategoryTranslationFields = [])
    {
        /** @var ProductCategoryTranslationRepository $productCategoryTranslationRepo */
        $productCategoryTranslationRepo = App::make(ProductCategoryTranslationRepository::class);
        $theme = $this->fakeProductCategoryTranslationData($productCategoryTranslationFields);
        return $productCategoryTranslationRepo->create($theme);
    }

    /**
     * Get fake instance of ProductCategoryTranslation
     *
     * @param array $productCategoryTranslationFields
     * @return ProductCategoryTranslation
     */
    public function fakeProductCategoryTranslation($productCategoryTranslationFields = [])
    {
        return new ProductCategoryTranslation($this->fakeProductCategoryTranslationData($productCategoryTranslationFields));
    }

    /**
     * Get fake data of ProductCategoryTranslation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeProductCategoryTranslationData($productCategoryTranslationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'product_category_id' => $fake->randomDigitNotNull,
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $productCategoryTranslationFields);
    }
}
