<?php

use Faker\Factory as Faker;
use App\Models\Admin\ServiceTranslation;
use App\Repositories\Admin\ServiceTranslationRepository;

trait MakeServiceTranslationTrait
{
    /**
     * Create fake instance of ServiceTranslation and save it in database
     *
     * @param array $serviceTranslationFields
     * @return ServiceTranslation
     */
    public function makeServiceTranslation($serviceTranslationFields = [])
    {
        /** @var ServiceTranslationRepository $serviceTranslationRepo */
        $serviceTranslationRepo = App::make(ServiceTranslationRepository::class);
        $theme = $this->fakeServiceTranslationData($serviceTranslationFields);
        return $serviceTranslationRepo->create($theme);
    }

    /**
     * Get fake instance of ServiceTranslation
     *
     * @param array $serviceTranslationFields
     * @return ServiceTranslation
     */
    public function fakeServiceTranslation($serviceTranslationFields = [])
    {
        return new ServiceTranslation($this->fakeServiceTranslationData($serviceTranslationFields));
    }

    /**
     * Get fake data of ServiceTranslation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeServiceTranslationData($serviceTranslationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'service_id' => $fake->randomDigitNotNull,
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $serviceTranslationFields);
    }
}
