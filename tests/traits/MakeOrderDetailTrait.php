<?php

use Faker\Factory as Faker;
use App\Models\Admin\OrderDetail;
use App\Repositories\Admin\OrderDetailRepository;

trait MakeOrderDetailTrait
{
    /**
     * Create fake instance of OrderDetail and save it in database
     *
     * @param array $orderDetailFields
     * @return OrderDetail
     */
    public function makeOrderDetail($orderDetailFields = [])
    {
        /** @var OrderDetailRepository $orderDetailRepo */
        $orderDetailRepo = App::make(OrderDetailRepository::class);
        $theme = $this->fakeOrderDetailData($orderDetailFields);
        return $orderDetailRepo->create($theme);
    }

    /**
     * Get fake instance of OrderDetail
     *
     * @param array $orderDetailFields
     * @return OrderDetail
     */
    public function fakeOrderDetail($orderDetailFields = [])
    {
        return new OrderDetail($this->fakeOrderDetailData($orderDetailFields));
    }

    /**
     * Get fake data of OrderDetail
     *
     * @param array $postFields
     * @return array
     */
    public function fakeOrderDetailData($orderDetailFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'order_id' => $fake->randomDigitNotNull,
            'product_presentation_id' => $fake->randomDigitNotNull,
            'products_amount' => $fake->randomDigitNotNull,
            'belongs_to_order_detail_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $orderDetailFields);
    }
}
