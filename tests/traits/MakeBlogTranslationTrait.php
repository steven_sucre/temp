<?php

use Faker\Factory as Faker;
use App\Models\Admin\BlogTranslation;
use App\Repositories\Admin\BlogTranslationRepository;

trait MakeBlogTranslationTrait
{
    /**
     * Create fake instance of BlogTranslation and save it in database
     *
     * @param array $blogTranslationFields
     * @return BlogTranslation
     */
    public function makeBlogTranslation($blogTranslationFields = [])
    {
        /** @var BlogTranslationRepository $blogTranslationRepo */
        $blogTranslationRepo = App::make(BlogTranslationRepository::class);
        $theme = $this->fakeBlogTranslationData($blogTranslationFields);
        return $blogTranslationRepo->create($theme);
    }

    /**
     * Get fake instance of BlogTranslation
     *
     * @param array $blogTranslationFields
     * @return BlogTranslation
     */
    public function fakeBlogTranslation($blogTranslationFields = [])
    {
        return new BlogTranslation($this->fakeBlogTranslationData($blogTranslationFields));
    }

    /**
     * Get fake data of BlogTranslation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBlogTranslationData($blogTranslationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'blog_id' => $fake->randomDigitNotNull,
            'language_id' => $fake->randomDigitNotNull,
            'title' => $fake->word,
            'subtitle' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $blogTranslationFields);
    }
}
