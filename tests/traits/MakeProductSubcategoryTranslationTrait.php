<?php

use Faker\Factory as Faker;
use App\Models\Admin\ProductSubcategoryTranslation;
use App\Repositories\Admin\ProductSubcategoryTranslationRepository;

trait MakeProductSubcategoryTranslationTrait
{
    /**
     * Create fake instance of ProductSubcategoryTranslation and save it in database
     *
     * @param array $productSubcategoryTranslationFields
     * @return ProductSubcategoryTranslation
     */
    public function makeProductSubcategoryTranslation($productSubcategoryTranslationFields = [])
    {
        /** @var ProductSubcategoryTranslationRepository $productSubcategoryTranslationRepo */
        $productSubcategoryTranslationRepo = App::make(ProductSubcategoryTranslationRepository::class);
        $theme = $this->fakeProductSubcategoryTranslationData($productSubcategoryTranslationFields);
        return $productSubcategoryTranslationRepo->create($theme);
    }

    /**
     * Get fake instance of ProductSubcategoryTranslation
     *
     * @param array $productSubcategoryTranslationFields
     * @return ProductSubcategoryTranslation
     */
    public function fakeProductSubcategoryTranslation($productSubcategoryTranslationFields = [])
    {
        return new ProductSubcategoryTranslation($this->fakeProductSubcategoryTranslationData($productSubcategoryTranslationFields));
    }

    /**
     * Get fake data of ProductSubcategoryTranslation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeProductSubcategoryTranslationData($productSubcategoryTranslationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'product_subcategory_id' => $fake->randomDigitNotNull,
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $productSubcategoryTranslationFields);
    }
}
