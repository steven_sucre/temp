<?php

use Faker\Factory as Faker;
use App\Models\Admin\RoomCategoryTranslation;
use App\Repositories\Admin\RoomCategoryTranslationRepository;

trait MakeRoomCategoryTranslationTrait
{
    /**
     * Create fake instance of RoomCategoryTranslation and save it in database
     *
     * @param array $roomCategoryTranslationFields
     * @return RoomCategoryTranslation
     */
    public function makeRoomCategoryTranslation($roomCategoryTranslationFields = [])
    {
        /** @var RoomCategoryTranslationRepository $roomCategoryTranslationRepo */
        $roomCategoryTranslationRepo = App::make(RoomCategoryTranslationRepository::class);
        $theme = $this->fakeRoomCategoryTranslationData($roomCategoryTranslationFields);
        return $roomCategoryTranslationRepo->create($theme);
    }

    /**
     * Get fake instance of RoomCategoryTranslation
     *
     * @param array $roomCategoryTranslationFields
     * @return RoomCategoryTranslation
     */
    public function fakeRoomCategoryTranslation($roomCategoryTranslationFields = [])
    {
        return new RoomCategoryTranslation($this->fakeRoomCategoryTranslationData($roomCategoryTranslationFields));
    }

    /**
     * Get fake data of RoomCategoryTranslation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRoomCategoryTranslationData($roomCategoryTranslationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'room_category_id' => $fake->randomDigitNotNull,
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $roomCategoryTranslationFields);
    }
}
