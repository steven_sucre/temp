<?php

use Faker\Factory as Faker;
use App\Models\Admin\Multimedia;
use App\Repositories\Admin\MultimediaRepository;

trait MakeMultimediaTrait
{
    /**
     * Create fake instance of Multimedia and save it in database
     *
     * @param array $multimediaFields
     * @return Multimedia
     */
    public function makeMultimedia($multimediaFields = [])
    {
        /** @var MultimediaRepository $multimediaRepo */
        $multimediaRepo = App::make(MultimediaRepository::class);
        $theme = $this->fakeMultimediaData($multimediaFields);
        return $multimediaRepo->create($theme);
    }

    /**
     * Get fake instance of Multimedia
     *
     * @param array $multimediaFields
     * @return Multimedia
     */
    public function fakeMultimedia($multimediaFields = [])
    {
        return new Multimedia($this->fakeMultimediaData($multimediaFields));
    }

    /**
     * Get fake data of Multimedia
     *
     * @param array $postFields
     * @return array
     */
    public function fakeMultimediaData($multimediaFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'slug' => $fake->word,
            'description' => $fake->word,
            'size' => $fake->randomDigitNotNull,
            'path' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $multimediaFields);
    }
}
