<?php

use Faker\Factory as Faker;
use App\Models\Admin\ServiceCategoryTranslation;
use App\Repositories\Admin\ServiceCategoryTranslationRepository;

trait MakeServiceCategoryTranslationTrait
{
    /**
     * Create fake instance of ServiceCategoryTranslation and save it in database
     *
     * @param array $serviceCategoryTranslationFields
     * @return ServiceCategoryTranslation
     */
    public function makeServiceCategoryTranslation($serviceCategoryTranslationFields = [])
    {
        /** @var ServiceCategoryTranslationRepository $serviceCategoryTranslationRepo */
        $serviceCategoryTranslationRepo = App::make(ServiceCategoryTranslationRepository::class);
        $theme = $this->fakeServiceCategoryTranslationData($serviceCategoryTranslationFields);
        return $serviceCategoryTranslationRepo->create($theme);
    }

    /**
     * Get fake instance of ServiceCategoryTranslation
     *
     * @param array $serviceCategoryTranslationFields
     * @return ServiceCategoryTranslation
     */
    public function fakeServiceCategoryTranslation($serviceCategoryTranslationFields = [])
    {
        return new ServiceCategoryTranslation($this->fakeServiceCategoryTranslationData($serviceCategoryTranslationFields));
    }

    /**
     * Get fake data of ServiceCategoryTranslation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeServiceCategoryTranslationData($serviceCategoryTranslationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'service_category_id' => $fake->randomDigitNotNull,
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $serviceCategoryTranslationFields);
    }
}
