<?php

use Faker\Factory as Faker;
use App\Models\Admin\BrandTranslation;
use App\Repositories\Admin\BrandTranslationRepository;

trait MakeBrandTranslationTrait
{
    /**
     * Create fake instance of BrandTranslation and save it in database
     *
     * @param array $brandTranslationFields
     * @return BrandTranslation
     */
    public function makeBrandTranslation($brandTranslationFields = [])
    {
        /** @var BrandTranslationRepository $brandTranslationRepo */
        $brandTranslationRepo = App::make(BrandTranslationRepository::class);
        $theme = $this->fakeBrandTranslationData($brandTranslationFields);
        return $brandTranslationRepo->create($theme);
    }

    /**
     * Get fake instance of BrandTranslation
     *
     * @param array $brandTranslationFields
     * @return BrandTranslation
     */
    public function fakeBrandTranslation($brandTranslationFields = [])
    {
        return new BrandTranslation($this->fakeBrandTranslationData($brandTranslationFields));
    }

    /**
     * Get fake data of BrandTranslation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBrandTranslationData($brandTranslationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'brand_id' => $fake->randomDigitNotNull,
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $brandTranslationFields);
    }
}
