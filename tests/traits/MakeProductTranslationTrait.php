<?php

use Faker\Factory as Faker;
use App\Models\Admin\ProductTranslation;
use App\Repositories\Admin\ProductTranslationRepository;

trait MakeProductTranslationTrait
{
    /**
     * Create fake instance of ProductTranslation and save it in database
     *
     * @param array $productTranslationFields
     * @return ProductTranslation
     */
    public function makeProductTranslation($productTranslationFields = [])
    {
        /** @var ProductTranslationRepository $productTranslationRepo */
        $productTranslationRepo = App::make(ProductTranslationRepository::class);
        $theme = $this->fakeProductTranslationData($productTranslationFields);
        return $productTranslationRepo->create($theme);
    }

    /**
     * Get fake instance of ProductTranslation
     *
     * @param array $productTranslationFields
     * @return ProductTranslation
     */
    public function fakeProductTranslation($productTranslationFields = [])
    {
        return new ProductTranslation($this->fakeProductTranslationData($productTranslationFields));
    }

    /**
     * Get fake data of ProductTranslation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeProductTranslationData($productTranslationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'product_id' => $fake->randomDigitNotNull,
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $productTranslationFields);
    }
}
