<?php

use Faker\Factory as Faker;
use App\Models\Admin\EventCategoryTranslation;
use App\Repositories\Admin\EventCategoryTranslationRepository;

trait MakeEventCategoryTranslationTrait
{
    /**
     * Create fake instance of EventCategoryTranslation and save it in database
     *
     * @param array $eventCategoryTranslationFields
     * @return EventCategoryTranslation
     */
    public function makeEventCategoryTranslation($eventCategoryTranslationFields = [])
    {
        /** @var EventCategoryTranslationRepository $eventCategoryTranslationRepo */
        $eventCategoryTranslationRepo = App::make(EventCategoryTranslationRepository::class);
        $theme = $this->fakeEventCategoryTranslationData($eventCategoryTranslationFields);
        return $eventCategoryTranslationRepo->create($theme);
    }

    /**
     * Get fake instance of EventCategoryTranslation
     *
     * @param array $eventCategoryTranslationFields
     * @return EventCategoryTranslation
     */
    public function fakeEventCategoryTranslation($eventCategoryTranslationFields = [])
    {
        return new EventCategoryTranslation($this->fakeEventCategoryTranslationData($eventCategoryTranslationFields));
    }

    /**
     * Get fake data of EventCategoryTranslation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeEventCategoryTranslationData($eventCategoryTranslationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'event_category_id' => $fake->randomDigitNotNull,
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $eventCategoryTranslationFields);
    }
}
