<?php

use Faker\Factory as Faker;
use App\Models\Admin\Booking;
use App\Repositories\Admin\BookingRepository;

trait MakeBookingTrait
{
    /**
     * Create fake instance of Booking and save it in database
     *
     * @param array $bookingFields
     * @return Booking
     */
    public function makeBooking($bookingFields = [])
    {
        /** @var BookingRepository $bookingRepo */
        $bookingRepo = App::make(BookingRepository::class);
        $theme = $this->fakeBookingData($bookingFields);
        return $bookingRepo->create($theme);
    }

    /**
     * Get fake instance of Booking
     *
     * @param array $bookingFields
     * @return Booking
     */
    public function fakeBooking($bookingFields = [])
    {
        return new Booking($this->fakeBookingData($bookingFields));
    }

    /**
     * Get fake data of Booking
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBookingData($bookingFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'subtotal' => $fake->randomDigitNotNull,
            'tax' => $fake->randomDigitNotNull,
            'total' => $fake->randomDigitNotNull,
            'user_id' => $fake->randomDigitNotNull,
            'status_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $bookingFields);
    }
}
