<?php

use Faker\Factory as Faker;
use App\Models\Admin\ActivityCategoryTranslation;
use App\Repositories\Admin\ActivityCategoryTranslationRepository;

trait MakeActivityCategoryTranslationTrait
{
    /**
     * Create fake instance of ActivityCategoryTranslation and save it in database
     *
     * @param array $activityCategoryTranslationFields
     * @return ActivityCategoryTranslation
     */
    public function makeActivityCategoryTranslation($activityCategoryTranslationFields = [])
    {
        /** @var ActivityCategoryTranslationRepository $activityCategoryTranslationRepo */
        $activityCategoryTranslationRepo = App::make(ActivityCategoryTranslationRepository::class);
        $theme = $this->fakeActivityCategoryTranslationData($activityCategoryTranslationFields);
        return $activityCategoryTranslationRepo->create($theme);
    }

    /**
     * Get fake instance of ActivityCategoryTranslation
     *
     * @param array $activityCategoryTranslationFields
     * @return ActivityCategoryTranslation
     */
    public function fakeActivityCategoryTranslation($activityCategoryTranslationFields = [])
    {
        return new ActivityCategoryTranslation($this->fakeActivityCategoryTranslationData($activityCategoryTranslationFields));
    }

    /**
     * Get fake data of ActivityCategoryTranslation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeActivityCategoryTranslationData($activityCategoryTranslationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'activity_category_id' => $fake->randomDigitNotNull,
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $activityCategoryTranslationFields);
    }
}
