<?php

use Faker\Factory as Faker;
use App\Models\Admin\RoomTranslation;
use App\Repositories\Admin\RoomTranslationRepository;

trait MakeRoomTranslationTrait
{
    /**
     * Create fake instance of RoomTranslation and save it in database
     *
     * @param array $roomTranslationFields
     * @return RoomTranslation
     */
    public function makeRoomTranslation($roomTranslationFields = [])
    {
        /** @var RoomTranslationRepository $roomTranslationRepo */
        $roomTranslationRepo = App::make(RoomTranslationRepository::class);
        $theme = $this->fakeRoomTranslationData($roomTranslationFields);
        return $roomTranslationRepo->create($theme);
    }

    /**
     * Get fake instance of RoomTranslation
     *
     * @param array $roomTranslationFields
     * @return RoomTranslation
     */
    public function fakeRoomTranslation($roomTranslationFields = [])
    {
        return new RoomTranslation($this->fakeRoomTranslationData($roomTranslationFields));
    }

    /**
     * Get fake data of RoomTranslation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRoomTranslationData($roomTranslationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'room_id' => $fake->randomDigitNotNull,
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $roomTranslationFields);
    }
}
