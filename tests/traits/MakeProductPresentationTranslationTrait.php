<?php

use Faker\Factory as Faker;
use App\Models\Admin\ProductPresentationTranslation;
use App\Repositories\Admin\ProductPresentationTranslationRepository;

trait MakeProductPresentationTranslationTrait
{
    /**
     * Create fake instance of ProductPresentationTranslation and save it in database
     *
     * @param array $productPresentationTranslationFields
     * @return ProductPresentationTranslation
     */
    public function makeProductPresentationTranslation($productPresentationTranslationFields = [])
    {
        /** @var ProductPresentationTranslationRepository $productPresentationTranslationRepo */
        $productPresentationTranslationRepo = App::make(ProductPresentationTranslationRepository::class);
        $theme = $this->fakeProductPresentationTranslationData($productPresentationTranslationFields);
        return $productPresentationTranslationRepo->create($theme);
    }

    /**
     * Get fake instance of ProductPresentationTranslation
     *
     * @param array $productPresentationTranslationFields
     * @return ProductPresentationTranslation
     */
    public function fakeProductPresentationTranslation($productPresentationTranslationFields = [])
    {
        return new ProductPresentationTranslation($this->fakeProductPresentationTranslationData($productPresentationTranslationFields));
    }

    /**
     * Get fake data of ProductPresentationTranslation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeProductPresentationTranslationData($productPresentationTranslationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'product_presentation_id' => $fake->randomDigitNotNull,
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'container' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $productPresentationTranslationFields);
    }
}
