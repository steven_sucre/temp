<?php

use Faker\Factory as Faker;
use App\Models\Admin\ProductPresentationProduct;
use App\Repositories\Admin\ProductPresentationProductRepository;

trait MakeProductPresentationProductTrait
{
    /**
     * Create fake instance of ProductPresentationProduct and save it in database
     *
     * @param array $productPresentationProductFields
     * @return ProductPresentationProduct
     */
    public function makeProductPresentationProduct($productPresentationProductFields = [])
    {
        /** @var ProductPresentationProductRepository $productPresentationProductRepo */
        $productPresentationProductRepo = App::make(ProductPresentationProductRepository::class);
        $theme = $this->fakeProductPresentationProductData($productPresentationProductFields);
        return $productPresentationProductRepo->create($theme);
    }

    /**
     * Get fake instance of ProductPresentationProduct
     *
     * @param array $productPresentationProductFields
     * @return ProductPresentationProduct
     */
    public function fakeProductPresentationProduct($productPresentationProductFields = [])
    {
        return new ProductPresentationProduct($this->fakeProductPresentationProductData($productPresentationProductFields));
    }

    /**
     * Get fake data of ProductPresentationProduct
     *
     * @param array $postFields
     * @return array
     */
    public function fakeProductPresentationProductData($productPresentationProductFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'product_presentation_id' => $fake->randomDigitNotNull,
            'product_id' => $fake->randomDigitNotNull,
            'code' => $fake->word,
            'slug' => $fake->word,
            'price' => $fake->randomDigitNotNull,
            'tax' => $fake->randomDigitNotNull,
            'product_quantity' => $fake->randomDigitNotNull,
            'for_delivery' => $fake->randomDigitNotNull,
            'max_quantity_of_sale' => $fake->randomDigitNotNull,
            'min_quantity_of_sale' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $productPresentationProductFields);
    }
}
