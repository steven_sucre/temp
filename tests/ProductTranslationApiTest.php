<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductTranslationApiTest extends TestCase
{
    use MakeProductTranslationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateProductTranslation()
    {
        $productTranslation = $this->fakeProductTranslationData();
        $this->json('POST', '/api/v1/productTranslations', $productTranslation);

        $this->assertApiResponse($productTranslation);
    }

    /**
     * @test
     */
    public function testReadProductTranslation()
    {
        $productTranslation = $this->makeProductTranslation();
        $this->json('GET', '/api/v1/productTranslations/'.$productTranslation->id);

        $this->assertApiResponse($productTranslation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateProductTranslation()
    {
        $productTranslation = $this->makeProductTranslation();
        $editedProductTranslation = $this->fakeProductTranslationData();

        $this->json('PUT', '/api/v1/productTranslations/'.$productTranslation->id, $editedProductTranslation);

        $this->assertApiResponse($editedProductTranslation);
    }

    /**
     * @test
     */
    public function testDeleteProductTranslation()
    {
        $productTranslation = $this->makeProductTranslation();
        $this->json('DELETE', '/api/v1/productTranslations/'.$productTranslation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/productTranslations/'.$productTranslation->id);

        $this->assertResponseStatus(404);
    }
}
