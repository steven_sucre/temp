<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductPresentationTranslationApiTest extends TestCase
{
    use MakeProductPresentationTranslationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateProductPresentationTranslation()
    {
        $productPresentationTranslation = $this->fakeProductPresentationTranslationData();
        $this->json('POST', '/api/v1/productPresentationTranslations', $productPresentationTranslation);

        $this->assertApiResponse($productPresentationTranslation);
    }

    /**
     * @test
     */
    public function testReadProductPresentationTranslation()
    {
        $productPresentationTranslation = $this->makeProductPresentationTranslation();
        $this->json('GET', '/api/v1/productPresentationTranslations/'.$productPresentationTranslation->id);

        $this->assertApiResponse($productPresentationTranslation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateProductPresentationTranslation()
    {
        $productPresentationTranslation = $this->makeProductPresentationTranslation();
        $editedProductPresentationTranslation = $this->fakeProductPresentationTranslationData();

        $this->json('PUT', '/api/v1/productPresentationTranslations/'.$productPresentationTranslation->id, $editedProductPresentationTranslation);

        $this->assertApiResponse($editedProductPresentationTranslation);
    }

    /**
     * @test
     */
    public function testDeleteProductPresentationTranslation()
    {
        $productPresentationTranslation = $this->makeProductPresentationTranslation();
        $this->json('DELETE', '/api/v1/productPresentationTranslations/'.$productPresentationTranslation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/productPresentationTranslations/'.$productPresentationTranslation->id);

        $this->assertResponseStatus(404);
    }
}
