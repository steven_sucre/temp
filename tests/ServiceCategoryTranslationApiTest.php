<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ServiceCategoryTranslationApiTest extends TestCase
{
    use MakeServiceCategoryTranslationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateServiceCategoryTranslation()
    {
        $serviceCategoryTranslation = $this->fakeServiceCategoryTranslationData();
        $this->json('POST', '/api/v1/serviceCategoryTranslations', $serviceCategoryTranslation);

        $this->assertApiResponse($serviceCategoryTranslation);
    }

    /**
     * @test
     */
    public function testReadServiceCategoryTranslation()
    {
        $serviceCategoryTranslation = $this->makeServiceCategoryTranslation();
        $this->json('GET', '/api/v1/serviceCategoryTranslations/'.$serviceCategoryTranslation->id);

        $this->assertApiResponse($serviceCategoryTranslation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateServiceCategoryTranslation()
    {
        $serviceCategoryTranslation = $this->makeServiceCategoryTranslation();
        $editedServiceCategoryTranslation = $this->fakeServiceCategoryTranslationData();

        $this->json('PUT', '/api/v1/serviceCategoryTranslations/'.$serviceCategoryTranslation->id, $editedServiceCategoryTranslation);

        $this->assertApiResponse($editedServiceCategoryTranslation);
    }

    /**
     * @test
     */
    public function testDeleteServiceCategoryTranslation()
    {
        $serviceCategoryTranslation = $this->makeServiceCategoryTranslation();
        $this->json('DELETE', '/api/v1/serviceCategoryTranslations/'.$serviceCategoryTranslation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/serviceCategoryTranslations/'.$serviceCategoryTranslation->id);

        $this->assertResponseStatus(404);
    }
}
