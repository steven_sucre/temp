<?php

use App\Models\Admin\ProductPresentationProduct;
use App\Repositories\Admin\ProductPresentationProductRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductPresentationProductRepositoryTest extends TestCase
{
    use MakeProductPresentationProductTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProductPresentationProductRepository
     */
    protected $productPresentationProductRepo;

    public function setUp()
    {
        parent::setUp();
        $this->productPresentationProductRepo = App::make(ProductPresentationProductRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateProductPresentationProduct()
    {
        $productPresentationProduct = $this->fakeProductPresentationProductData();
        $createdProductPresentationProduct = $this->productPresentationProductRepo->create($productPresentationProduct);
        $createdProductPresentationProduct = $createdProductPresentationProduct->toArray();
        $this->assertArrayHasKey('id', $createdProductPresentationProduct);
        $this->assertNotNull($createdProductPresentationProduct['id'], 'Created ProductPresentationProduct must have id specified');
        $this->assertNotNull(ProductPresentationProduct::find($createdProductPresentationProduct['id']), 'ProductPresentationProduct with given id must be in DB');
        $this->assertModelData($productPresentationProduct, $createdProductPresentationProduct);
    }

    /**
     * @test read
     */
    public function testReadProductPresentationProduct()
    {
        $productPresentationProduct = $this->makeProductPresentationProduct();
        $dbProductPresentationProduct = $this->productPresentationProductRepo->find($productPresentationProduct->id);
        $dbProductPresentationProduct = $dbProductPresentationProduct->toArray();
        $this->assertModelData($productPresentationProduct->toArray(), $dbProductPresentationProduct);
    }

    /**
     * @test update
     */
    public function testUpdateProductPresentationProduct()
    {
        $productPresentationProduct = $this->makeProductPresentationProduct();
        $fakeProductPresentationProduct = $this->fakeProductPresentationProductData();
        $updatedProductPresentationProduct = $this->productPresentationProductRepo->update($fakeProductPresentationProduct, $productPresentationProduct->id);
        $this->assertModelData($fakeProductPresentationProduct, $updatedProductPresentationProduct->toArray());
        $dbProductPresentationProduct = $this->productPresentationProductRepo->find($productPresentationProduct->id);
        $this->assertModelData($fakeProductPresentationProduct, $dbProductPresentationProduct->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteProductPresentationProduct()
    {
        $productPresentationProduct = $this->makeProductPresentationProduct();
        $resp = $this->productPresentationProductRepo->delete($productPresentationProduct->id);
        $this->assertTrue($resp);
        $this->assertNull(ProductPresentationProduct::find($productPresentationProduct->id), 'ProductPresentationProduct should not exist in DB');
    }
}
