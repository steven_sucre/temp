<?php

use App\Models\Admin\ServiceCategoryTranslation;
use App\Repositories\Admin\ServiceCategoryTranslationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ServiceCategoryTranslationRepositoryTest extends TestCase
{
    use MakeServiceCategoryTranslationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ServiceCategoryTranslationRepository
     */
    protected $serviceCategoryTranslationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->serviceCategoryTranslationRepo = App::make(ServiceCategoryTranslationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateServiceCategoryTranslation()
    {
        $serviceCategoryTranslation = $this->fakeServiceCategoryTranslationData();
        $createdServiceCategoryTranslation = $this->serviceCategoryTranslationRepo->create($serviceCategoryTranslation);
        $createdServiceCategoryTranslation = $createdServiceCategoryTranslation->toArray();
        $this->assertArrayHasKey('id', $createdServiceCategoryTranslation);
        $this->assertNotNull($createdServiceCategoryTranslation['id'], 'Created ServiceCategoryTranslation must have id specified');
        $this->assertNotNull(ServiceCategoryTranslation::find($createdServiceCategoryTranslation['id']), 'ServiceCategoryTranslation with given id must be in DB');
        $this->assertModelData($serviceCategoryTranslation, $createdServiceCategoryTranslation);
    }

    /**
     * @test read
     */
    public function testReadServiceCategoryTranslation()
    {
        $serviceCategoryTranslation = $this->makeServiceCategoryTranslation();
        $dbServiceCategoryTranslation = $this->serviceCategoryTranslationRepo->find($serviceCategoryTranslation->id);
        $dbServiceCategoryTranslation = $dbServiceCategoryTranslation->toArray();
        $this->assertModelData($serviceCategoryTranslation->toArray(), $dbServiceCategoryTranslation);
    }

    /**
     * @test update
     */
    public function testUpdateServiceCategoryTranslation()
    {
        $serviceCategoryTranslation = $this->makeServiceCategoryTranslation();
        $fakeServiceCategoryTranslation = $this->fakeServiceCategoryTranslationData();
        $updatedServiceCategoryTranslation = $this->serviceCategoryTranslationRepo->update($fakeServiceCategoryTranslation, $serviceCategoryTranslation->id);
        $this->assertModelData($fakeServiceCategoryTranslation, $updatedServiceCategoryTranslation->toArray());
        $dbServiceCategoryTranslation = $this->serviceCategoryTranslationRepo->find($serviceCategoryTranslation->id);
        $this->assertModelData($fakeServiceCategoryTranslation, $dbServiceCategoryTranslation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteServiceCategoryTranslation()
    {
        $serviceCategoryTranslation = $this->makeServiceCategoryTranslation();
        $resp = $this->serviceCategoryTranslationRepo->delete($serviceCategoryTranslation->id);
        $this->assertTrue($resp);
        $this->assertNull(ServiceCategoryTranslation::find($serviceCategoryTranslation->id), 'ServiceCategoryTranslation should not exist in DB');
    }
}
