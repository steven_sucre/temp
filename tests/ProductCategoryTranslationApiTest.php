<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductCategoryTranslationApiTest extends TestCase
{
    use MakeProductCategoryTranslationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateProductCategoryTranslation()
    {
        $productCategoryTranslation = $this->fakeProductCategoryTranslationData();
        $this->json('POST', '/api/v1/productCategoryTranslations', $productCategoryTranslation);

        $this->assertApiResponse($productCategoryTranslation);
    }

    /**
     * @test
     */
    public function testReadProductCategoryTranslation()
    {
        $productCategoryTranslation = $this->makeProductCategoryTranslation();
        $this->json('GET', '/api/v1/productCategoryTranslations/'.$productCategoryTranslation->id);

        $this->assertApiResponse($productCategoryTranslation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateProductCategoryTranslation()
    {
        $productCategoryTranslation = $this->makeProductCategoryTranslation();
        $editedProductCategoryTranslation = $this->fakeProductCategoryTranslationData();

        $this->json('PUT', '/api/v1/productCategoryTranslations/'.$productCategoryTranslation->id, $editedProductCategoryTranslation);

        $this->assertApiResponse($editedProductCategoryTranslation);
    }

    /**
     * @test
     */
    public function testDeleteProductCategoryTranslation()
    {
        $productCategoryTranslation = $this->makeProductCategoryTranslation();
        $this->json('DELETE', '/api/v1/productCategoryTranslations/'.$productCategoryTranslation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/productCategoryTranslations/'.$productCategoryTranslation->id);

        $this->assertResponseStatus(404);
    }
}
