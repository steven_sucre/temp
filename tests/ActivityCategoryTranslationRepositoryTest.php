<?php

use App\Models\Admin\ActivityCategoryTranslation;
use App\Repositories\Admin\ActivityCategoryTranslationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ActivityCategoryTranslationRepositoryTest extends TestCase
{
    use MakeActivityCategoryTranslationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ActivityCategoryTranslationRepository
     */
    protected $activityCategoryTranslationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->activityCategoryTranslationRepo = App::make(ActivityCategoryTranslationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateActivityCategoryTranslation()
    {
        $activityCategoryTranslation = $this->fakeActivityCategoryTranslationData();
        $createdActivityCategoryTranslation = $this->activityCategoryTranslationRepo->create($activityCategoryTranslation);
        $createdActivityCategoryTranslation = $createdActivityCategoryTranslation->toArray();
        $this->assertArrayHasKey('id', $createdActivityCategoryTranslation);
        $this->assertNotNull($createdActivityCategoryTranslation['id'], 'Created ActivityCategoryTranslation must have id specified');
        $this->assertNotNull(ActivityCategoryTranslation::find($createdActivityCategoryTranslation['id']), 'ActivityCategoryTranslation with given id must be in DB');
        $this->assertModelData($activityCategoryTranslation, $createdActivityCategoryTranslation);
    }

    /**
     * @test read
     */
    public function testReadActivityCategoryTranslation()
    {
        $activityCategoryTranslation = $this->makeActivityCategoryTranslation();
        $dbActivityCategoryTranslation = $this->activityCategoryTranslationRepo->find($activityCategoryTranslation->id);
        $dbActivityCategoryTranslation = $dbActivityCategoryTranslation->toArray();
        $this->assertModelData($activityCategoryTranslation->toArray(), $dbActivityCategoryTranslation);
    }

    /**
     * @test update
     */
    public function testUpdateActivityCategoryTranslation()
    {
        $activityCategoryTranslation = $this->makeActivityCategoryTranslation();
        $fakeActivityCategoryTranslation = $this->fakeActivityCategoryTranslationData();
        $updatedActivityCategoryTranslation = $this->activityCategoryTranslationRepo->update($fakeActivityCategoryTranslation, $activityCategoryTranslation->id);
        $this->assertModelData($fakeActivityCategoryTranslation, $updatedActivityCategoryTranslation->toArray());
        $dbActivityCategoryTranslation = $this->activityCategoryTranslationRepo->find($activityCategoryTranslation->id);
        $this->assertModelData($fakeActivityCategoryTranslation, $dbActivityCategoryTranslation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteActivityCategoryTranslation()
    {
        $activityCategoryTranslation = $this->makeActivityCategoryTranslation();
        $resp = $this->activityCategoryTranslationRepo->delete($activityCategoryTranslation->id);
        $this->assertTrue($resp);
        $this->assertNull(ActivityCategoryTranslation::find($activityCategoryTranslation->id), 'ActivityCategoryTranslation should not exist in DB');
    }
}
