<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BlogTranslationApiTest extends TestCase
{
    use MakeBlogTranslationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBlogTranslation()
    {
        $blogTranslation = $this->fakeBlogTranslationData();
        $this->json('POST', '/api/v1/blogTranslations', $blogTranslation);

        $this->assertApiResponse($blogTranslation);
    }

    /**
     * @test
     */
    public function testReadBlogTranslation()
    {
        $blogTranslation = $this->makeBlogTranslation();
        $this->json('GET', '/api/v1/blogTranslations/'.$blogTranslation->id);

        $this->assertApiResponse($blogTranslation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBlogTranslation()
    {
        $blogTranslation = $this->makeBlogTranslation();
        $editedBlogTranslation = $this->fakeBlogTranslationData();

        $this->json('PUT', '/api/v1/blogTranslations/'.$blogTranslation->id, $editedBlogTranslation);

        $this->assertApiResponse($editedBlogTranslation);
    }

    /**
     * @test
     */
    public function testDeleteBlogTranslation()
    {
        $blogTranslation = $this->makeBlogTranslation();
        $this->json('DELETE', '/api/v1/blogTranslations/'.$blogTranslation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/blogTranslations/'.$blogTranslation->id);

        $this->assertResponseStatus(404);
    }
}
