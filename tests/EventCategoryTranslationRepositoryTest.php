<?php

use App\Models\Admin\EventCategoryTranslation;
use App\Repositories\Admin\EventCategoryTranslationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EventCategoryTranslationRepositoryTest extends TestCase
{
    use MakeEventCategoryTranslationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var EventCategoryTranslationRepository
     */
    protected $eventCategoryTranslationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->eventCategoryTranslationRepo = App::make(EventCategoryTranslationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateEventCategoryTranslation()
    {
        $eventCategoryTranslation = $this->fakeEventCategoryTranslationData();
        $createdEventCategoryTranslation = $this->eventCategoryTranslationRepo->create($eventCategoryTranslation);
        $createdEventCategoryTranslation = $createdEventCategoryTranslation->toArray();
        $this->assertArrayHasKey('id', $createdEventCategoryTranslation);
        $this->assertNotNull($createdEventCategoryTranslation['id'], 'Created EventCategoryTranslation must have id specified');
        $this->assertNotNull(EventCategoryTranslation::find($createdEventCategoryTranslation['id']), 'EventCategoryTranslation with given id must be in DB');
        $this->assertModelData($eventCategoryTranslation, $createdEventCategoryTranslation);
    }

    /**
     * @test read
     */
    public function testReadEventCategoryTranslation()
    {
        $eventCategoryTranslation = $this->makeEventCategoryTranslation();
        $dbEventCategoryTranslation = $this->eventCategoryTranslationRepo->find($eventCategoryTranslation->id);
        $dbEventCategoryTranslation = $dbEventCategoryTranslation->toArray();
        $this->assertModelData($eventCategoryTranslation->toArray(), $dbEventCategoryTranslation);
    }

    /**
     * @test update
     */
    public function testUpdateEventCategoryTranslation()
    {
        $eventCategoryTranslation = $this->makeEventCategoryTranslation();
        $fakeEventCategoryTranslation = $this->fakeEventCategoryTranslationData();
        $updatedEventCategoryTranslation = $this->eventCategoryTranslationRepo->update($fakeEventCategoryTranslation, $eventCategoryTranslation->id);
        $this->assertModelData($fakeEventCategoryTranslation, $updatedEventCategoryTranslation->toArray());
        $dbEventCategoryTranslation = $this->eventCategoryTranslationRepo->find($eventCategoryTranslation->id);
        $this->assertModelData($fakeEventCategoryTranslation, $dbEventCategoryTranslation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteEventCategoryTranslation()
    {
        $eventCategoryTranslation = $this->makeEventCategoryTranslation();
        $resp = $this->eventCategoryTranslationRepo->delete($eventCategoryTranslation->id);
        $this->assertTrue($resp);
        $this->assertNull(EventCategoryTranslation::find($eventCategoryTranslation->id), 'EventCategoryTranslation should not exist in DB');
    }
}
