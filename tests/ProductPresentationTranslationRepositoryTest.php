<?php

use App\Models\Admin\ProductPresentationTranslation;
use App\Repositories\Admin\ProductPresentationTranslationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductPresentationTranslationRepositoryTest extends TestCase
{
    use MakeProductPresentationTranslationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProductPresentationTranslationRepository
     */
    protected $productPresentationTranslationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->productPresentationTranslationRepo = App::make(ProductPresentationTranslationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateProductPresentationTranslation()
    {
        $productPresentationTranslation = $this->fakeProductPresentationTranslationData();
        $createdProductPresentationTranslation = $this->productPresentationTranslationRepo->create($productPresentationTranslation);
        $createdProductPresentationTranslation = $createdProductPresentationTranslation->toArray();
        $this->assertArrayHasKey('id', $createdProductPresentationTranslation);
        $this->assertNotNull($createdProductPresentationTranslation['id'], 'Created ProductPresentationTranslation must have id specified');
        $this->assertNotNull(ProductPresentationTranslation::find($createdProductPresentationTranslation['id']), 'ProductPresentationTranslation with given id must be in DB');
        $this->assertModelData($productPresentationTranslation, $createdProductPresentationTranslation);
    }

    /**
     * @test read
     */
    public function testReadProductPresentationTranslation()
    {
        $productPresentationTranslation = $this->makeProductPresentationTranslation();
        $dbProductPresentationTranslation = $this->productPresentationTranslationRepo->find($productPresentationTranslation->id);
        $dbProductPresentationTranslation = $dbProductPresentationTranslation->toArray();
        $this->assertModelData($productPresentationTranslation->toArray(), $dbProductPresentationTranslation);
    }

    /**
     * @test update
     */
    public function testUpdateProductPresentationTranslation()
    {
        $productPresentationTranslation = $this->makeProductPresentationTranslation();
        $fakeProductPresentationTranslation = $this->fakeProductPresentationTranslationData();
        $updatedProductPresentationTranslation = $this->productPresentationTranslationRepo->update($fakeProductPresentationTranslation, $productPresentationTranslation->id);
        $this->assertModelData($fakeProductPresentationTranslation, $updatedProductPresentationTranslation->toArray());
        $dbProductPresentationTranslation = $this->productPresentationTranslationRepo->find($productPresentationTranslation->id);
        $this->assertModelData($fakeProductPresentationTranslation, $dbProductPresentationTranslation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteProductPresentationTranslation()
    {
        $productPresentationTranslation = $this->makeProductPresentationTranslation();
        $resp = $this->productPresentationTranslationRepo->delete($productPresentationTranslation->id);
        $this->assertTrue($resp);
        $this->assertNull(ProductPresentationTranslation::find($productPresentationTranslation->id), 'ProductPresentationTranslation should not exist in DB');
    }
}
