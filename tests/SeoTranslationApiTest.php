<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SeoTranslationApiTest extends TestCase
{
    use MakeSeoTranslationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSeoTranslation()
    {
        $seoTranslation = $this->fakeSeoTranslationData();
        $this->json('POST', '/api/v1/seoTranslations', $seoTranslation);

        $this->assertApiResponse($seoTranslation);
    }

    /**
     * @test
     */
    public function testReadSeoTranslation()
    {
        $seoTranslation = $this->makeSeoTranslation();
        $this->json('GET', '/api/v1/seoTranslations/'.$seoTranslation->id);

        $this->assertApiResponse($seoTranslation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSeoTranslation()
    {
        $seoTranslation = $this->makeSeoTranslation();
        $editedSeoTranslation = $this->fakeSeoTranslationData();

        $this->json('PUT', '/api/v1/seoTranslations/'.$seoTranslation->id, $editedSeoTranslation);

        $this->assertApiResponse($editedSeoTranslation);
    }

    /**
     * @test
     */
    public function testDeleteSeoTranslation()
    {
        $seoTranslation = $this->makeSeoTranslation();
        $this->json('DELETE', '/api/v1/seoTranslations/'.$seoTranslation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/seoTranslations/'.$seoTranslation->id);

        $this->assertResponseStatus(404);
    }
}
