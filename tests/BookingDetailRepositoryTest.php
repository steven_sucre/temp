<?php

use App\Models\Admin\BookingDetail;
use App\Repositories\Admin\BookingDetailRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BookingDetailRepositoryTest extends TestCase
{
    use MakeBookingDetailTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BookingDetailRepository
     */
    protected $bookingDetailRepo;

    public function setUp()
    {
        parent::setUp();
        $this->bookingDetailRepo = App::make(BookingDetailRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBookingDetail()
    {
        $bookingDetail = $this->fakeBookingDetailData();
        $createdBookingDetail = $this->bookingDetailRepo->create($bookingDetail);
        $createdBookingDetail = $createdBookingDetail->toArray();
        $this->assertArrayHasKey('id', $createdBookingDetail);
        $this->assertNotNull($createdBookingDetail['id'], 'Created BookingDetail must have id specified');
        $this->assertNotNull(BookingDetail::find($createdBookingDetail['id']), 'BookingDetail with given id must be in DB');
        $this->assertModelData($bookingDetail, $createdBookingDetail);
    }

    /**
     * @test read
     */
    public function testReadBookingDetail()
    {
        $bookingDetail = $this->makeBookingDetail();
        $dbBookingDetail = $this->bookingDetailRepo->find($bookingDetail->id);
        $dbBookingDetail = $dbBookingDetail->toArray();
        $this->assertModelData($bookingDetail->toArray(), $dbBookingDetail);
    }

    /**
     * @test update
     */
    public function testUpdateBookingDetail()
    {
        $bookingDetail = $this->makeBookingDetail();
        $fakeBookingDetail = $this->fakeBookingDetailData();
        $updatedBookingDetail = $this->bookingDetailRepo->update($fakeBookingDetail, $bookingDetail->id);
        $this->assertModelData($fakeBookingDetail, $updatedBookingDetail->toArray());
        $dbBookingDetail = $this->bookingDetailRepo->find($bookingDetail->id);
        $this->assertModelData($fakeBookingDetail, $dbBookingDetail->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBookingDetail()
    {
        $bookingDetail = $this->makeBookingDetail();
        $resp = $this->bookingDetailRepo->delete($bookingDetail->id);
        $this->assertTrue($resp);
        $this->assertNull(BookingDetail::find($bookingDetail->id), 'BookingDetail should not exist in DB');
    }
}
