<?php

use App\Models\Admin\RoomSeasonTranslation;
use App\Repositories\Admin\RoomSeasonTranslationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoomSeasonTranslationRepositoryTest extends TestCase
{
    use MakeRoomSeasonTranslationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RoomSeasonTranslationRepository
     */
    protected $roomSeasonTranslationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->roomSeasonTranslationRepo = App::make(RoomSeasonTranslationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateRoomSeasonTranslation()
    {
        $roomSeasonTranslation = $this->fakeRoomSeasonTranslationData();
        $createdRoomSeasonTranslation = $this->roomSeasonTranslationRepo->create($roomSeasonTranslation);
        $createdRoomSeasonTranslation = $createdRoomSeasonTranslation->toArray();
        $this->assertArrayHasKey('id', $createdRoomSeasonTranslation);
        $this->assertNotNull($createdRoomSeasonTranslation['id'], 'Created RoomSeasonTranslation must have id specified');
        $this->assertNotNull(RoomSeasonTranslation::find($createdRoomSeasonTranslation['id']), 'RoomSeasonTranslation with given id must be in DB');
        $this->assertModelData($roomSeasonTranslation, $createdRoomSeasonTranslation);
    }

    /**
     * @test read
     */
    public function testReadRoomSeasonTranslation()
    {
        $roomSeasonTranslation = $this->makeRoomSeasonTranslation();
        $dbRoomSeasonTranslation = $this->roomSeasonTranslationRepo->find($roomSeasonTranslation->id);
        $dbRoomSeasonTranslation = $dbRoomSeasonTranslation->toArray();
        $this->assertModelData($roomSeasonTranslation->toArray(), $dbRoomSeasonTranslation);
    }

    /**
     * @test update
     */
    public function testUpdateRoomSeasonTranslation()
    {
        $roomSeasonTranslation = $this->makeRoomSeasonTranslation();
        $fakeRoomSeasonTranslation = $this->fakeRoomSeasonTranslationData();
        $updatedRoomSeasonTranslation = $this->roomSeasonTranslationRepo->update($fakeRoomSeasonTranslation, $roomSeasonTranslation->id);
        $this->assertModelData($fakeRoomSeasonTranslation, $updatedRoomSeasonTranslation->toArray());
        $dbRoomSeasonTranslation = $this->roomSeasonTranslationRepo->find($roomSeasonTranslation->id);
        $this->assertModelData($fakeRoomSeasonTranslation, $dbRoomSeasonTranslation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteRoomSeasonTranslation()
    {
        $roomSeasonTranslation = $this->makeRoomSeasonTranslation();
        $resp = $this->roomSeasonTranslationRepo->delete($roomSeasonTranslation->id);
        $this->assertTrue($resp);
        $this->assertNull(RoomSeasonTranslation::find($roomSeasonTranslation->id), 'RoomSeasonTranslation should not exist in DB');
    }
}
