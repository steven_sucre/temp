<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ServiceTranslationApiTest extends TestCase
{
    use MakeServiceTranslationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateServiceTranslation()
    {
        $serviceTranslation = $this->fakeServiceTranslationData();
        $this->json('POST', '/api/v1/serviceTranslations', $serviceTranslation);

        $this->assertApiResponse($serviceTranslation);
    }

    /**
     * @test
     */
    public function testReadServiceTranslation()
    {
        $serviceTranslation = $this->makeServiceTranslation();
        $this->json('GET', '/api/v1/serviceTranslations/'.$serviceTranslation->id);

        $this->assertApiResponse($serviceTranslation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateServiceTranslation()
    {
        $serviceTranslation = $this->makeServiceTranslation();
        $editedServiceTranslation = $this->fakeServiceTranslationData();

        $this->json('PUT', '/api/v1/serviceTranslations/'.$serviceTranslation->id, $editedServiceTranslation);

        $this->assertApiResponse($editedServiceTranslation);
    }

    /**
     * @test
     */
    public function testDeleteServiceTranslation()
    {
        $serviceTranslation = $this->makeServiceTranslation();
        $this->json('DELETE', '/api/v1/serviceTranslations/'.$serviceTranslation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/serviceTranslations/'.$serviceTranslation->id);

        $this->assertResponseStatus(404);
    }
}
