<?php

use App\Models\Admin\SeoTranslation;
use App\Repositories\Admin\SeoTranslationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SeoTranslationRepositoryTest extends TestCase
{
    use MakeSeoTranslationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var SeoTranslationRepository
     */
    protected $seoTranslationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->seoTranslationRepo = App::make(SeoTranslationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSeoTranslation()
    {
        $seoTranslation = $this->fakeSeoTranslationData();
        $createdSeoTranslation = $this->seoTranslationRepo->create($seoTranslation);
        $createdSeoTranslation = $createdSeoTranslation->toArray();
        $this->assertArrayHasKey('id', $createdSeoTranslation);
        $this->assertNotNull($createdSeoTranslation['id'], 'Created SeoTranslation must have id specified');
        $this->assertNotNull(SeoTranslation::find($createdSeoTranslation['id']), 'SeoTranslation with given id must be in DB');
        $this->assertModelData($seoTranslation, $createdSeoTranslation);
    }

    /**
     * @test read
     */
    public function testReadSeoTranslation()
    {
        $seoTranslation = $this->makeSeoTranslation();
        $dbSeoTranslation = $this->seoTranslationRepo->find($seoTranslation->id);
        $dbSeoTranslation = $dbSeoTranslation->toArray();
        $this->assertModelData($seoTranslation->toArray(), $dbSeoTranslation);
    }

    /**
     * @test update
     */
    public function testUpdateSeoTranslation()
    {
        $seoTranslation = $this->makeSeoTranslation();
        $fakeSeoTranslation = $this->fakeSeoTranslationData();
        $updatedSeoTranslation = $this->seoTranslationRepo->update($fakeSeoTranslation, $seoTranslation->id);
        $this->assertModelData($fakeSeoTranslation, $updatedSeoTranslation->toArray());
        $dbSeoTranslation = $this->seoTranslationRepo->find($seoTranslation->id);
        $this->assertModelData($fakeSeoTranslation, $dbSeoTranslation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSeoTranslation()
    {
        $seoTranslation = $this->makeSeoTranslation();
        $resp = $this->seoTranslationRepo->delete($seoTranslation->id);
        $this->assertTrue($resp);
        $this->assertNull(SeoTranslation::find($seoTranslation->id), 'SeoTranslation should not exist in DB');
    }
}
