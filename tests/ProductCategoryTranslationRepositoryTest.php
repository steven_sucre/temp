<?php

use App\Models\Admin\ProductCategoryTranslation;
use App\Repositories\Admin\ProductCategoryTranslationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductCategoryTranslationRepositoryTest extends TestCase
{
    use MakeProductCategoryTranslationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProductCategoryTranslationRepository
     */
    protected $productCategoryTranslationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->productCategoryTranslationRepo = App::make(ProductCategoryTranslationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateProductCategoryTranslation()
    {
        $productCategoryTranslation = $this->fakeProductCategoryTranslationData();
        $createdProductCategoryTranslation = $this->productCategoryTranslationRepo->create($productCategoryTranslation);
        $createdProductCategoryTranslation = $createdProductCategoryTranslation->toArray();
        $this->assertArrayHasKey('id', $createdProductCategoryTranslation);
        $this->assertNotNull($createdProductCategoryTranslation['id'], 'Created ProductCategoryTranslation must have id specified');
        $this->assertNotNull(ProductCategoryTranslation::find($createdProductCategoryTranslation['id']), 'ProductCategoryTranslation with given id must be in DB');
        $this->assertModelData($productCategoryTranslation, $createdProductCategoryTranslation);
    }

    /**
     * @test read
     */
    public function testReadProductCategoryTranslation()
    {
        $productCategoryTranslation = $this->makeProductCategoryTranslation();
        $dbProductCategoryTranslation = $this->productCategoryTranslationRepo->find($productCategoryTranslation->id);
        $dbProductCategoryTranslation = $dbProductCategoryTranslation->toArray();
        $this->assertModelData($productCategoryTranslation->toArray(), $dbProductCategoryTranslation);
    }

    /**
     * @test update
     */
    public function testUpdateProductCategoryTranslation()
    {
        $productCategoryTranslation = $this->makeProductCategoryTranslation();
        $fakeProductCategoryTranslation = $this->fakeProductCategoryTranslationData();
        $updatedProductCategoryTranslation = $this->productCategoryTranslationRepo->update($fakeProductCategoryTranslation, $productCategoryTranslation->id);
        $this->assertModelData($fakeProductCategoryTranslation, $updatedProductCategoryTranslation->toArray());
        $dbProductCategoryTranslation = $this->productCategoryTranslationRepo->find($productCategoryTranslation->id);
        $this->assertModelData($fakeProductCategoryTranslation, $dbProductCategoryTranslation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteProductCategoryTranslation()
    {
        $productCategoryTranslation = $this->makeProductCategoryTranslation();
        $resp = $this->productCategoryTranslationRepo->delete($productCategoryTranslation->id);
        $this->assertTrue($resp);
        $this->assertNull(ProductCategoryTranslation::find($productCategoryTranslation->id), 'ProductCategoryTranslation should not exist in DB');
    }
}
