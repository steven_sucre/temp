<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoomSeasonTranslationApiTest extends TestCase
{
    use MakeRoomSeasonTranslationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateRoomSeasonTranslation()
    {
        $roomSeasonTranslation = $this->fakeRoomSeasonTranslationData();
        $this->json('POST', '/api/v1/roomSeasonTranslations', $roomSeasonTranslation);

        $this->assertApiResponse($roomSeasonTranslation);
    }

    /**
     * @test
     */
    public function testReadRoomSeasonTranslation()
    {
        $roomSeasonTranslation = $this->makeRoomSeasonTranslation();
        $this->json('GET', '/api/v1/roomSeasonTranslations/'.$roomSeasonTranslation->id);

        $this->assertApiResponse($roomSeasonTranslation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateRoomSeasonTranslation()
    {
        $roomSeasonTranslation = $this->makeRoomSeasonTranslation();
        $editedRoomSeasonTranslation = $this->fakeRoomSeasonTranslationData();

        $this->json('PUT', '/api/v1/roomSeasonTranslations/'.$roomSeasonTranslation->id, $editedRoomSeasonTranslation);

        $this->assertApiResponse($editedRoomSeasonTranslation);
    }

    /**
     * @test
     */
    public function testDeleteRoomSeasonTranslation()
    {
        $roomSeasonTranslation = $this->makeRoomSeasonTranslation();
        $this->json('DELETE', '/api/v1/roomSeasonTranslations/'.$roomSeasonTranslation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/roomSeasonTranslations/'.$roomSeasonTranslation->id);

        $this->assertResponseStatus(404);
    }
}
