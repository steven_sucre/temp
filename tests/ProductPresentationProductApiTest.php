<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductPresentationProductApiTest extends TestCase
{
    use MakeProductPresentationProductTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateProductPresentationProduct()
    {
        $productPresentationProduct = $this->fakeProductPresentationProductData();
        $this->json('POST', '/api/v1/productPresentationProducts', $productPresentationProduct);

        $this->assertApiResponse($productPresentationProduct);
    }

    /**
     * @test
     */
    public function testReadProductPresentationProduct()
    {
        $productPresentationProduct = $this->makeProductPresentationProduct();
        $this->json('GET', '/api/v1/productPresentationProducts/'.$productPresentationProduct->id);

        $this->assertApiResponse($productPresentationProduct->toArray());
    }

    /**
     * @test
     */
    public function testUpdateProductPresentationProduct()
    {
        $productPresentationProduct = $this->makeProductPresentationProduct();
        $editedProductPresentationProduct = $this->fakeProductPresentationProductData();

        $this->json('PUT', '/api/v1/productPresentationProducts/'.$productPresentationProduct->id, $editedProductPresentationProduct);

        $this->assertApiResponse($editedProductPresentationProduct);
    }

    /**
     * @test
     */
    public function testDeleteProductPresentationProduct()
    {
        $productPresentationProduct = $this->makeProductPresentationProduct();
        $this->json('DELETE', '/api/v1/productPresentationProducts/'.$productPresentationProduct->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/productPresentationProducts/'.$productPresentationProduct->id);

        $this->assertResponseStatus(404);
    }
}
