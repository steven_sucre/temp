<?php

use App\Models\Admin\BlogCategoryTranslation;
use App\Repositories\Admin\BlogCategoryTranslationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BlogCategoryTranslationRepositoryTest extends TestCase
{
    use MakeBlogCategoryTranslationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BlogCategoryTranslationRepository
     */
    protected $blogCategoryTranslationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->blogCategoryTranslationRepo = App::make(BlogCategoryTranslationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBlogCategoryTranslation()
    {
        $blogCategoryTranslation = $this->fakeBlogCategoryTranslationData();
        $createdBlogCategoryTranslation = $this->blogCategoryTranslationRepo->create($blogCategoryTranslation);
        $createdBlogCategoryTranslation = $createdBlogCategoryTranslation->toArray();
        $this->assertArrayHasKey('id', $createdBlogCategoryTranslation);
        $this->assertNotNull($createdBlogCategoryTranslation['id'], 'Created BlogCategoryTranslation must have id specified');
        $this->assertNotNull(BlogCategoryTranslation::find($createdBlogCategoryTranslation['id']), 'BlogCategoryTranslation with given id must be in DB');
        $this->assertModelData($blogCategoryTranslation, $createdBlogCategoryTranslation);
    }

    /**
     * @test read
     */
    public function testReadBlogCategoryTranslation()
    {
        $blogCategoryTranslation = $this->makeBlogCategoryTranslation();
        $dbBlogCategoryTranslation = $this->blogCategoryTranslationRepo->find($blogCategoryTranslation->id);
        $dbBlogCategoryTranslation = $dbBlogCategoryTranslation->toArray();
        $this->assertModelData($blogCategoryTranslation->toArray(), $dbBlogCategoryTranslation);
    }

    /**
     * @test update
     */
    public function testUpdateBlogCategoryTranslation()
    {
        $blogCategoryTranslation = $this->makeBlogCategoryTranslation();
        $fakeBlogCategoryTranslation = $this->fakeBlogCategoryTranslationData();
        $updatedBlogCategoryTranslation = $this->blogCategoryTranslationRepo->update($fakeBlogCategoryTranslation, $blogCategoryTranslation->id);
        $this->assertModelData($fakeBlogCategoryTranslation, $updatedBlogCategoryTranslation->toArray());
        $dbBlogCategoryTranslation = $this->blogCategoryTranslationRepo->find($blogCategoryTranslation->id);
        $this->assertModelData($fakeBlogCategoryTranslation, $dbBlogCategoryTranslation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBlogCategoryTranslation()
    {
        $blogCategoryTranslation = $this->makeBlogCategoryTranslation();
        $resp = $this->blogCategoryTranslationRepo->delete($blogCategoryTranslation->id);
        $this->assertTrue($resp);
        $this->assertNull(BlogCategoryTranslation::find($blogCategoryTranslation->id), 'BlogCategoryTranslation should not exist in DB');
    }
}
