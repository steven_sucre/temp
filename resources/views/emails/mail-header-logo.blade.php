<table class="row footer text-center" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:center;vertical-align:top;width:100%">
    <tbody style="background-color:#070707;">
        <tr style="padding:0;text-align:left;vertical-align:top">
            <th class="small-12 large-3 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-left:16px;padding-right:8px;padding-top:16px;text-align:left;width:129px">
                <p class="text-center" style="Margin:0;Margin-bottom:10px;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:center"><b></b></p>
                <center data-parsed="" style="min-width:97px;width:100%"><img src="{{ asset('images/header/logo-1.png') }}" style="-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</center>
            </th>
        </tr>
    </tbody>
</table>