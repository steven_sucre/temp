<table class="row footer text-center" style="background-color:#070707;border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:center;vertical-align:top;width:100%">
    <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <th class="small-12 large-3 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-left:16px;padding-right:8px;padding-top:16px;text-align:left;width:129px">
                <p class="text-center" style="Margin:0;Margin-bottom:10px;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:center">
                  <b>© All Rights Reserved | ByNight 2018 | <a href="#">Privacy Policy</a> | <a href="#">Termini e Condizioni</a> | <a href="#">Annulla l'iscrizione</a>
                  </b>
                </p>
                <center data-parsed="" style="min-width:97px;width:100%"><a href="http://jumperr.com"><img width="160px" height="43px" src="{{ asset('images/powered-by1.png') }}" style="-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto"></a></center>
            </th>
        </tr>
    </tbody>
</table>