@extends('layouts.app')
@section('content')
    <navheader></navheader>
    <router-view></router-view>
@endsection
@section('scripts')